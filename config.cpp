#include <opencv2/opencv.hpp>

#include <fstream>

#include "config.hpp"

using namespace std;

namespace em {


ConfigElement::operator const std::string ()
{
	return value;
}

string ConfigElement::toString ()
{
	return value;
}

int ConfigElement::toInt ()
{
	return atoi (value.c_str());
}

float ConfigElement::toFloat ()
{
	return atof (value.c_str());
}

void ConfigString::addElement (const string& element)
{
	elements.push_back (new ConfigElement (element));
}

ConfigElement& ConfigString::operator [] (int n)
{
	if (n < 0 || n >= elements.size())
		throw ConfigIndexException ("No element found\n");

	return *elements[n];
}

int ConfigString::size ()
{
	return elements.size();
}

void ConfigSection::addString (ConfigString* str)
{
	strings.push_back (str);
}

const string& ConfigSection::getName ()
{
	return name;
}

ConfigString& ConfigSection::operator [] (int n)
{
	if (n < 0 || n >= strings.size())
		throw ConfigIndexException ("No config string found\n");

	return *strings[n];
}

ConfigString& ConfigSection::operator [] (const string& name)
{
	ConfigString* st = NULL;

	for (vector<ConfigString*>::iterator it = strings.begin(); it != strings.end(); it ++)
		if (name == (string)((*it)->operator[](0))) {
			st = *it;
			break;
		}

	if (!st)
		throw ConfigIndexException ("No config string with name '" + name + "'\n");

	return *st;
}

bool ConfigSection::exists (const string& name)
{
	for (vector<ConfigString*>::iterator it = strings.begin(); it != strings.end(); it ++)
		if (name == (string)((*it)->operator[](0)))
			return true;

	return false;
}

int ConfigSection::size ()
{
	return strings.size();
}

class StringTokenizer {
private:
	string str;
	int pos;
public:
	StringTokenizer (const string& str) : str (str), pos (0) {}
	bool nextToken (string& t);
};

bool StringTokenizer::nextToken (string& t)
{
	bool end = false;
	bool begin = false;
	bool dquote = false;

	if (pos >= str.size())
		return false;

	ostringstream st;

	while (!end) {
		char c = str[pos];
		
		switch (c) {
			case '"':
				dquote = !dquote;
				begin = true;
				break;

			case ';':
				if (!dquote) {
					pos = str.size() - 1;
					end = true;
					break;
				}
			case ' ':
			case '\t':
				if (dquote) {
					st.put (c);
					break;
				}
				if (!begin)
					break;
			case '\n':
			case '\r':
			case '\0':
				end = true;
				break;
			default:
				st.put (c);
				begin = true;
		}

		if (++pos >= str.size())
			end = true;
	}

	string s = st.str();
	if (s.size() == 0)
		return false;

	t = s;

	return true;
}

Config::Config (const string& filename)
{
	load (filename);
}

void Config::load (const std::string& filename)
{
	string line, t;
	ifstream fs (filename.c_str(), ifstream::in);

	ConfigSection* sec = NULL;

	while (fs.good()) {
		getline (fs, line);

		StringTokenizer st (line);

		if (st.nextToken (t)) {
			if (t[0] == '@') {
				if (sec) 
					sections.push_back (sec);
				
				sec = new ConfigSection (t.substr(1));
				continue;
			}
		} else
			continue;

		if (!sec)
			throw ConfigParseException ("No section name!\n");

		ConfigString* cst = new ConfigString;

		do {
			cst->addElement (t);
		} while (st.nextToken (t));

		sec->addString (cst);
	}

	if (sec)
		sections.push_back (sec);

	fs.close();
}

ConfigSection& Config::operator [] (int n)
{
	if (n < 0 || n >= sections.size())
		throw ConfigIndexException ("No section found\n");

	return *sections[n];
}

ConfigSection& Config::operator [] (const string& name)
{
	ConfigSection* sec = NULL;

	for (vector<ConfigSection*>::iterator it = sections.begin(); it != sections.end(); it ++)
		if (name == (*it)->getName()) {
			sec = *it;
			break;
		}

	if (!sec)
		throw ConfigIndexException ("No section with name '" + name + "'\n");

	return *sec;
}

bool Config::exists (const string& name)
{
	for (vector<ConfigSection*>::iterator it = sections.begin(); it != sections.end(); it ++)
		if (name == (*it)->getName())
			return true;

	return false;
}

int Config::size ()
{
	return sections.size();
}

void Config::debugPrint ()
{
	cout << "size: " << size() << endl;
	for (vector<ConfigSection*>::iterator isec = sections.begin(); isec != sections.end(); isec ++) {
		cout << "SECTION \"" << (*isec)->name << "\"" << endl;
		cout << "    size: " << (*isec)->size() << endl;
		for (vector<ConfigString*>::iterator istr = (*isec)->strings.begin(); istr != (*isec)->strings.end(); istr ++) {
			cout << "    (" << (*istr)->size() << ") ";
			for (vector<ConfigElement*>::iterator iel = (*istr)->elements.begin(); iel != (*istr)->elements.end(); iel ++)
				cout << "\""<< (string)(*(*iel)) << "\" ";
			cout << endl;
		}
	}
}

} /* namespace em */
