#ifndef CONFIG_HPP
#define CONFIG_HPP 

#include <string>
#include <vector>

//using namespace std;

namespace em {

class ConfigException {
public:
	std::string message;
	ConfigException (const std::string& message) {
		this->message = "CONFIG: " + message;
	}
};

class ConfigIndexException : public ConfigException {
public:
	ConfigIndexException (const std::string& message) : ConfigException (message) {}
};

class ConfigParseException : public ConfigException {
public:
	ConfigParseException (const std::string& message) : ConfigException (message) {}
};


class ConfigElement {
private:
	std::string value;

	ConfigElement (const std::string& value) : value (value) {}
public:
	friend class ConfigString;

	operator const std::string ();

	std::string toString ();
	int toInt ();
	float toFloat ();
};

class ConfigString {
private:
	std::vector<ConfigElement*> elements;

	ConfigString () {}
	void addElement (const std::string& element);
public:
	friend class Config;

	ConfigElement& operator [] (int n);
	int size ();
};

class ConfigSection {
private:
	std::string name;
	std::vector<ConfigString*> strings;

	ConfigSection (const std::string& name) : name (name) {}
	void addString (ConfigString* str);
public:
	friend class Config;

	const std::string& getName ();

	ConfigString& operator [] (int n);
	ConfigString& operator [] (const std::string& name);

	bool exists (const std::string& name);
	int size ();
};

class Config {
private:
	std::vector<ConfigSection*> sections;

public:
	Config () {}
	Config (const std::string& filename);

	void load (const std::string& filename);

	ConfigSection& operator [] (int n);
	ConfigSection& operator [] (const std::string& name);

	bool exists (const std::string& name);
	int size ();

	void debugPrint ();
};

} /* namespace em */

#endif /* CONFIG_HPP */
