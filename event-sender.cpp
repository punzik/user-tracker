#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "event-sender.hpp"

#define EVENT_SENDER_DEBUG

namespace em {

EventSender::EventSender ()
{
	sock = -1;
}

EventSender::~EventSender ()
{
	if (isConnected())
		disconnect();
}


/* creates a TCP socket, connects it ot the server and returns it's ID */
bool EventSender::connect (const char* addr, int port)
{
	struct sockaddr_in sa;
	int s;
	int rc;

	s = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (s == -1) {
		return false;
	}

	memset(&sa, 0, sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);

	rc = inet_pton(AF_INET, addr, &sa.sin_addr);
	if (rc <= 0) {
		close(s);
		return false;
	}

	if (::connect(s, (struct sockaddr *)&sa, sizeof(sa)) != 0) {
		close(s);
		return false;
	}

	sock = s;

	return true;
}

/* terminates the TCP connection and the socket */
void EventSender::disconnect ()
{
	if (sock != -1) {
		shutdown (sock, SHUT_RDWR);
		close (sock);
		sock = -1;
	}
}

void EventSender::mouseMove (int x, int y)
{
	char buf[512];
	snprintf (buf, 512, "X:%d  Y:%d", x, y);
#ifdef EVENT_SENDER_DEBUG
	printf ("%s\n", buf);
#endif
	send (sock, buf, strlen(buf) + 1, 0);
}

void EventSender::mouseBPress (int button)
{
	char buf[512];
	snprintf (buf, 512, "PRESSED:%d", button);
#ifdef EVENT_SENDER_DEBUG
	printf ("%s\n", buf);
#endif
	send (sock, buf, strlen(buf) + 1, 0);
}

void EventSender::mouseBRelease (int button)
{
	char buf[512];
	snprintf (buf, 512, "RELEASED:%d", button);
#ifdef EVENT_SENDER_DEBUG
	printf ("%s\n", buf);
#endif
	send (sock, buf, strlen(buf) + 1, 0);
}

void EventSender::sendKey (char c)
{
	char buf[512];
	snprintf (buf, 512, "key:0x%.2x", (int)c);
#ifdef EVENT_SENDER_DEBUG
	printf ("%s\n", buf);
#endif
	send (sock, buf, strlen(buf) + 1, 0);
}

void EventSender::pressKey (char c)
{
	char buf[512];
	snprintf (buf, 512, "key_push:0x%.2x", (int)c);
#ifdef EVENT_SENDER_DEBUG
	printf ("%s\n", buf);
#endif
	send (sock, buf, strlen(buf) + 1, 0);
}

void EventSender::releaseKey (char c)
{
	char buf[512];
	snprintf (buf, 512, "key_release:0x%.2x", (int)c);
#ifdef EVENT_SENDER_DEBUG
	printf ("%s\n", buf);
#endif
	send (sock, buf, strlen(buf) + 1, 0);
}

bool EventSender::isConnected ()
{
	return (sock != -1);
}

} /* namespace em; */
