#ifndef EVENT_SENDER_HPP
#define EVENT_SENDER_HPP 

namespace em {

class EventSender {
protected:
	int sock;
public:
	EventSender ();
	~EventSender ();

	bool connect (const char* addr, int port);
	void disconnect ();

	void mouseMove (int x, int y);
	void mouseBPress (int button);
	void mouseBRelease (int button);
	void sendKey (char c);
	void pressKey (char c);
	void releaseKey (char c);

	bool isConnected ();
};

} /* namespace em; */

#endif /* EVENT_SENDER_HPP */
