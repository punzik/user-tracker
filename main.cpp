/* (C) 2011 Nikolay Puzanov <punzik@gmail.com>, Artem Bagautdinov <artem.bagautdinov@gmail.com> */

#include <vector>
#include <opencv2/opencv.hpp>

#include "skeleton.hpp"
#include "event-sender.hpp"
#include "config.hpp"

using namespace std;

struct KeyAlias {
	string alias;
	char code;
};

static const char keyMap[256] = {
/*	0     1     2     3     4     5     6     7     8     9     a     b     c     d     e     f     */
/* 0 */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 1 */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 2 */	0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 3 */	0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 4 */	0x00, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f,
/* 5 */	0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 6 */	0x00, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f,
/* 7 */	0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 8 */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* 9 */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* a */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* b */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* c */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* d */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* e */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* f */	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

static const int keyAliasesCount = 22;
static const KeyAlias keyAliases[] = {
	{ "enter",	0x0d },
	{ "backspace",	0x08 },
	{ "tab",	0x09 },
	{ "arrow-left", 0x25 },
	{ "arrow-right", 0x27 },
	{ "arrow-up",	0x26 },
	{ "arrow-down", 0x28 },
	{ "escape",	0x1b },
	{ "page-up",	0x21 },
	{ "page-down",	0x22 },
	{ "f1",		0x70 },
	{ "f2",		0x71 },
	{ "f3",		0x72 },
	{ "f4",		0x73 },
	{ "f5",		0x74 },
	{ "f6",		0x75 },
	{ "f7",		0x76 },
	{ "f8",		0x77 },
	{ "f9",		0x78 },
	{ "f10",	0x79 },
	{ "f11",	0x7a },
	{ "f12",	0x7b },
};

/* GLOBAL configuration structure */
em::Config conf;
unsigned long frame = 0;
unsigned int maxDistance = 0;
bool usersExists = false;

static set<string> entered;

static void setUsersActivity (vector<em::User*> users)
{
	bool om = conf["system"].exists("only-main-user");

	for (vector<em::User*>::iterator it = users.begin(); it != users.end(); it ++)
		/*
		if (it == users.begin() && (maxDistance == 0 || (*it)->getMeanZ() < maxDistance))
			(*it)->setActive();
		else
			(*it)->setInactive();
		*/
		if ((*it)->getMeanZ() < maxDistance || ((*it)->isTop() && om))
			(*it)->setActive();
		else
			(*it)->setInactive();
}

static char keyToKeycode (string key)
{
	char keyCode = 0;

	if (key.size() > 1) {
		if (key.size() > 2 && key[0] == '0' && (key[1] == 'x' || key[1] == 'X')) {
			/* HEX */
			keyCode = (char) strtol (key.substr (2).c_str(), NULL, 16);
		} else {
			/* ALIAS */
			for (int i = 0; i < keyAliasesCount; i ++)
				if (keyAliases[i].alias == key) {
					keyCode = keyAliases[i].code;
					break;
				}
		}
	} else
		/* MAP */ 
		keyCode = keyMap[(unsigned char)key[0]];

	return keyCode;
}

static void userLost (em::User* user, void* priv)
{
	cout << "User lost" << std::endl;

	for (set<string>::iterator it = entered.begin(); it != entered.end(); it ++) {
		if (conf["events"].exists(*it)) {
			string key = conf["events"][*it][2].toString();
			((em::EventSender*) priv)->releaseKey (keyToKeycode (key));
		}
	}
	
	entered.clear();
}

void handleAction (em::User* user, em::EventSender* sender, const string& sid, bool holded = false, bool enter = true)
{
	static unsigned long prevActionFrame = 0;
	
	if (conf["events"].exists(sid))
		if (conf["events"][sid][1].toString() == "set-main")
			if (!user->isTop()) {
				user->setTop();
				if (conf["events"][sid].size() == 3) {
					char keyCode = keyToKeycode (conf["events"][sid][2].toString());
					sender->sendKey (keyCode);
				}
			}

	if (conf["system"].exists("only-main-user") && !user->isTop())
		return;
	
	bool handled = false;
	
	for (int i = 0; i < conf["events"].size(); i ++)
		if (conf["events"][i][0].toString() == "double")
		{
			int timeout = conf["events"][i][3].toInt ();
			if ((frame - prevActionFrame) > timeout)
				continue;

			string ev1 = conf["events"][i][1].toString ();
			string ev2 = conf["events"][i][2].toString ();
			bool ev1e = (entered.find(ev1) != entered.end());
			bool ev2e = (entered.find(ev2) != entered.end());

			if ((ev1 == sid && ev2e) || (ev2 == sid && ev1e))
			{
				string action = conf["events"][i][4].toString ();
				string key = conf["events"][i][5].toString ();
				char keyCode = keyToKeycode (key);
				
				cout << "double action: " << ev1 << "+" << ev2 << endl;

				/* Release unused keys */
				if (ev1e && conf["events"].exists(ev1))
					sender->releaseKey (keyToKeycode (conf["events"][ev1][2].toString()));
				if (ev2e && conf["events"].exists(ev2))
					sender->releaseKey (keyToKeycode (conf["events"][ev2][2].toString()));
				
				if (action == "key-hold") {
					if (enter)
						sender->pressKey (keyCode);
					else {
						sender->releaseKey (keyCode);
						entered.erase (ev1);
						entered.erase (ev2);
					}
				} else
				if (action == "key-press" && enter) {
					sender->sendKey (keyCode);
					entered.erase (ev1);
					entered.erase (ev2);
				} else
					cout << "WARNING: unknown action " << action << endl;
				
				handled = true;
				break;
			}
		}
        
	if (!handled)
		for (int i = 0; i < conf["events"].size(); i ++) {
			em::ConfigString& cs = conf["events"][i];
			
			if ((cs[0].toString() == sid) && (cs[1].toString() != "set-main"))
			{
				string action = cs[1].toString();
				string key = cs[2].toString();
				char keyCode = keyToKeycode (key);

				if (action == "key-hold") {
					if (!holded) {
						cout << "WARNING: not allowed key-hold action in this context. Just press '" << key << "'" << endl;
						sender->sendKey (keyCode);
					} else
						if (enter) {
							entered.insert (sid);
							sender->pressKey (keyCode);
						} else
							if (entered.erase (sid) > 0)
								sender->releaseKey (keyCode);
				} else
				if (action == "key-press" && enter) {
					sender->sendKey (keyCode);
				} else
					cout << "WARNING: unknown action " << action << endl;
			}
		}

	prevActionFrame = frame;
}

void gestureReceived (em::User* user, const vector<cv::Point3f>& gesture, void* priv)
{
	cout << "Receive gesture for " << gesture.size() << " points" << endl;
}

void gestureRecognized (em::User* user, const string& sid, float likelihood, float error, void* priv)
{
	cout << "Gesture: " << sid << " (" << likelihood << "," << error << ")" << endl;
	
	handleAction (user, (em::EventSender*) priv, sid);
}

void inLimitCallback (em::User* user, const std::string& sid, bool inLimit, void* priv)
{
	cout << "Limit: " << sid << " " << (inLimit ? "enter" : "exit") << endl;

	handleAction (user, (em::EventSender*) priv, sid, true, inLimit);
}

void poseDetect (em::User* user, void* priv)
{
	cout << "Pose..." << endl;
}

void poseRecognized (em::User* user, const string& sid, void* priv)
{
	cout << "Pose " << sid << endl;
}

void handIsUp (em::User* user, bool left, void* priv)
{
	cout << (left ? "LEFT" : "RIGHT") << " hand is up" << endl;

	handleAction (user, (em::EventSender*) priv, (left ? "lh-handup" : "rh-handup"));
}

void usersPresent (void* priv)
{
	em::EventSender* sender = (em::EventSender*) priv;
	
	cout << "Users present" << endl;
	
	char key = keyToKeycode (conf["events"]["users-present"][2].toString());
	sender->sendKey (key);
	
	usersExists = true;
}

void usersAbsent (void* priv)
{
	em::EventSender* sender = (em::EventSender*) priv;

	cout << "Users absent" << endl;
	
	char key = keyToKeycode (conf["events"]["users-absent"][2].toString());
	sender->sendKey (key);

	usersExists = false;
}


#define DRAW_TIME	15
cv::Mat keypoints;
int drawTimer = 0;

static void drawKeyPoint (em::User* user, const cv::Point& point, void* priv)
{
	cv::circle (keypoints, point, 5, cv::Scalar (0,0,255), 2);
	drawTimer = DRAW_TIME;
}

static em::SkelPoint strToSkelPoint (const string& str)
{
	if (str == "left-hand")
		return em::SP_L_HAND;
	else
	if (str == "right-hand")
		return em::SP_R_HAND;
	else
	if (str == "left-elbow")
		return em::SP_L_ELBOW;
	else
	if (str == "right-elbow")
		return em::SP_R_ELBOW;
	else
	if (str == "left-shoulder")
		return em::SP_L_SHOULDER;
	else
	if (str == "right-shoulder")
		return em::SP_R_SHOULDER;
	else
	if (str == "head")
		return em::SP_HEAD;
	else
	if (str == "neck")
		return em::SP_NECK;
	else
	if (str == "torso")
		return em::SP_TORSO;
	else
		throw new em::ConfigParseException ("Unknown point '" + str + "' \n");
}

static int strToBorder (const string& str)
{
	if (str == "up")
		return em::BorderLimitRecognizer::LIMIT_UP;
	else
	if (str == "down")
		return em::BorderLimitRecognizer::LIMIT_DOWN;
	else
	if (str == "left")
		return em::BorderLimitRecognizer::LIMIT_LEFT;
	else
	if (str == "right")
		return em::BorderLimitRecognizer::LIMIT_RIGHT;
	else
	if (str == "forward")
		return em::BorderLimitRecognizer::LIMIT_FORWARD;
	else
		throw new em::ConfigParseException ("Unknown border '" + str + "' \n");
}

int
main (int argc, char** argv)
{
	if (argc < 2 || argc > 3) {
		cout << argv[0] << " <config-file.cfg> [record-file]" << endl << endl;
		return -1;
	}

	/* Load config */
	try {
		conf.load (argv[1]);

		bool mirror = conf["system"].exists ("mirror");
		string data_path = conf["system"]["data-path"][1].toString();
		if (conf["system"].exists ("max-distance"))
			maxDistance = conf["system"]["max-distance"][1].toInt ();

		/* Connect to events receiver */
		em::EventSender evSender;

		const char* serverAddr = conf["system"]["ev-server-addr"][1].toString().c_str();
		int serverPort = conf["system"]["ev-server-port"][1].toInt();
		if (!evSender.connect (serverAddr, serverPort)) {
			cout << "Can't connect to " << serverAddr << ":" << serverPort << endl;
			cout << "Continue without sending events to remote host!" << endl;
		}

		/* Create Kinect controller */
		if (argc == 3) cout << "Load recording " << argv[2] << endl;
		string userCalibration = data_path + "/" + conf["system"]["user-calibration"][1].toString();
		em::KinectController ctrl (conf["system"]["max-active-users"][1].toInt(), userCalibration, argc == 3 ? argv[2] : NULL);
		ctrl.setMirror (mirror);
		ctrl.setUsersActivityCallback (setUsersActivity);
		ctrl.setUserLostCallback (userLost, &evSender);

		for (int pn = 0; pn < conf["processors"].size(); pn ++) {
			em::ConfigString& proc = conf["processors"][pn];
			if (proc[0].toString() == "proc-zg") {
				int gzl_enter = proc[1].toInt();
				int gzl_exit = proc[2].toInt();
				em::TrainableZGestureRecognizer* gestRecog = new em::TrainableZGestureRecognizer (gzl_enter, gzl_exit);
				gestRecog->setGestureReceivedCB (gestureReceived, (void*)&evSender);
				gestRecog->setGestureRecognizedCB (gestureRecognized, (void*)&evSender);
				gestRecog->setDrawKeyPointCB (drawKeyPoint, NULL);

				em::ConfigSection& trSec = conf[proc[3].toString()];
				for (int i = 0; i < trSec.size(); i ++) {
					string sid = trSec[i][0].toString();
					em::SkelPoint point = strToSkelPoint (trSec[i][1].toString());
					string trainFile = trSec[i][2].toString();

					gestRecog->loadTrainingSet (data_path + "/" + trainFile, point, sid);
				}

				ctrl.addRecognizer (gestRecog);
			} else
			if (proc[0].toString() == "proc-ob") {
				em::BorderLimitRecognizer* borderRecog = new em::BorderLimitRecognizer;
				borderRecog->setInLimitCB (inLimitCallback, (void*)&evSender);
				borderRecog->setDrawKeyPointCB (drawKeyPoint, NULL);

				em::ConfigSection& sec = conf[proc[1].toString()];
				for (int i = 0; i < sec.size(); i ++) {
					string sid = sec[i][0].toString();
					em::SkelPoint point = strToSkelPoint (sec[i][1].toString());
					int border = strToBorder (sec[i][2].toString());
					float th_down = sec[i][3].toFloat();
					float th_up = sec[i][4].toFloat();
					int timeout = sec[i][5].toInt();
					bool nostop = false;
					
					if (sec[i].size() == 7 && sec[i][6].toString() == "nostop")
						nostop = true;

					borderRecog->addLimit (border, th_down, th_up, timeout, nostop, point, sid);
					cout << "add limit " << sid << endl;
				}

				ctrl.addRecognizer (borderRecog);
			} else
			if (proc[0].toString() == "proc-pose-rhu") {
				em::TrainablePoseRecognizer* poseRhu = new em::TrainablePoseRecognizer;
				
				poseRhu->setPoseDetectCB (poseDetect, 15, 0.1, NULL);
				poseRhu->setPoseRecognizedCB (poseRecognized, NULL);
				
				vector<pair<em::SkelAngle, cv::Point2f> > pose_av;
				vector<pair<em::SkelAngle, cv::Point2f> > pose_re;
				/*
				     [ SA_L_FOREARM SA_L_SHOULDER ]
				av = [ 1.35456 1.47059 2.96522 2.54766 ]
				re = [ 0.207932 0.124965 0.173308 0.755683 ]
				*/
				pose_av.push_back (pair<em::SkelAngle, cv::Point2f> (em::SA_L_FOREARM, cv::Point2f (1.35456, 1.47059)));
				pose_av.push_back (pair<em::SkelAngle, cv::Point2f> (em::SA_L_SHOULDER, cv::Point2f (2.96522, 2.54766)));
				//pose_re.push_back (pair<em::SkelAngle, cv::Point2f> (em::SA_L_FOREARM, cv::Point2f (0.207932, 0.124965)));
				//pose_re.push_back (pair<em::SkelAngle, cv::Point2f> (em::SA_L_SHOULDER, cv::Point2f (0.173308, 0.755683)));
				pose_re.push_back (pair<em::SkelAngle, cv::Point2f> (em::SA_L_FOREARM, cv::Point2f (0.4, 0.4)));
				pose_re.push_back (pair<em::SkelAngle, cv::Point2f> (em::SA_L_SHOULDER, cv::Point2f (0.4, 0.4)));
				poseRhu->loadTrainingSet (pose_av, pose_re, "pose-rhu");
				
				ctrl.addRecognizer (poseRhu);
			} else
			if (proc[0].toString() == "proc-handup") {
				em::HandUpRecognizer* handupRecog = new em::HandUpRecognizer ();
				handupRecog->setHandIsUpCB (handIsUp, (void*)&evSender);
				handupRecog->setDrawKeyPointCB (drawKeyPoint, NULL);
				handupRecog->setTimeout (proc[1].toInt ());
				
				ctrl.addRecognizer (handupRecog);
			} else
			if (proc[0].toString() == "proc-pose") {
				cout << "WARNING: Pose processor not implemented yet!" << endl;
			} else
				throw new em::ConfigParseException ("unknown processor '" + proc[0].toString() + "'\n");
		}
		
		if (conf["events"].exists("users-present"))
			ctrl.setUsersPresentCallback (usersPresent, conf["events"]["users-present"][1].toInt(), (void*)&evSender);
		if (conf["events"].exists("users-absent"))
			ctrl.setUsersAbsentCallback (usersAbsent, conf["events"]["users-absent"][1].toInt(), (void*)&evSender);

		bool doRecord = conf["system"].exists("record");
		string recFile;
		if (doRecord)
			recFile = conf["system"]["record"][1].toString();
		
		/* Start control */
		ctrl.startControl();

		/* Init GUI matrices */
		cv::Mat depth;
		cv::Mat depth_8u;
		cv::Mat usersImage;
		cv::Mat umask;
		cv::Mat recordImage;

		cv::Size depthSize = ctrl.getDepthSize();
		cv::Size imageSize = ctrl.getImageSize();

		keypoints.create (depthSize, CV_8UC3);
		usersImage.create (depthSize, CV_8UC3);
		umask.create (depthSize, CV_8UC1);

		cv::VideoWriter vOut;
		if (doRecord)
			if (!vOut.open(recFile, CV_FOURCC('D','I','V','X'), 30, imageSize, true)) {
				cout << "ERROR: Cannot open " << recFile << " for write video!" << endl;
				doRecord = false;
			}
		
		while (true) {
			if (!ctrl.processFrame())
				break;

			frame = ctrl.getFrameNumber ();

			ctrl.getImage (recordImage);
			if (doRecord && usersExists) {
				vOut << recordImage;
				cv::circle (recordImage, cv::Point (20,20), 15, cv::Scalar (0,0,255), 3);
			}
			
			ctrl.getDepth (depth);
			depth.convertTo (depth_8u, CV_8UC1, 1./32.);
			cv::cvtColor (depth_8u, depth, CV_GRAY2BGR);

			umask.setTo (cv::Scalar (0));
			usersImage.setTo (cv::Scalar (0,0,0));

			vector<em::User*> users = ctrl.getUsers ();
			for (vector<em::User*>::iterator it = users.begin(); it != users.end(); it ++) {
				em::User* user = *it;
				if (user->isTracking())
					umask.setTo (cv::Scalar(1), user->getMask());
			}

			if (drawTimer == 0)
				keypoints.setTo (cv::Scalar (0,0,0));
			else
				drawTimer --;

			depth.copyTo (usersImage, umask);
			usersImage += keypoints;

			//cv::imshow ("depth", depth);
			cv::imshow ("image", recordImage);
			cv::imshow ("users", usersImage);

			if ((char)cv::waitKey(5) == 'q')
				break;
		}

	} catch (em::ConfigException ex) {
		cout << ex.message << endl;
		return -1;
	} catch (XnStatus err) {
		cout << xnGetStatusString (err) << endl;
		return -1;
	} catch (const char* msg) {
		cout << msg << endl;
		return -1;
	}

	cout << "Exiting with return code 0" << endl;

	return 0;
}
