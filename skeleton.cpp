#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

#include "skeleton.hpp"

using namespace std;

namespace em {

/**
 * Set user activity callback STUB.
 * Activate nearest user, deactivate other.
 */
static void setUsersActivityStub (vector<User*> users)
{
	for (vector<User*>::iterator it = users.begin(); it != users.end(); it ++)
		if (it == users.begin())
			(*it)->setActive();
		else
			(*it)->setInactive();
}

static float vectorLength (const cv::Point3f& vec)
{
	float dx, dy, dz, l;

	dx = fabs (vec.x);
	dy = fabs (vec.y);
	dz = fabs (vec.z);

	if (!isnormal (dx)) dx = 0;
	if (!isnormal (dy)) dy = 0;
	if (!isnormal (dz)) dz = 0;

	if (dx < 1 || dy < 1)
		l = dx + dy;
	else
		l = sqrt (dx * dx + dy * dy);
	
	if (l < 1 || dz < 1)
		l = l + dz;
	else
		l = sqrt (l * l + dz * dz);

	return l;
}

/**
 * Convert xnPoint3D to cv::Point3f
 */
cv::Point3f xnPoint3d_to_cvPoint3f (XnPoint3D& xp)
{
	cv::Point3f cp;

	cp.x = xp.X;
	cp.y = xp.Y;
	cp.z = xp.Z;

	return cp;
}

/**
 * Convert cv::Point3f to xnPoint3D
 */
XnPoint3D cvPoint3f_to_xnPoint3d (cv::Point3f& cp)
{
	XnPoint3D xp;

	xp.X = cp.x;
	xp.Y = cp.y;
	xp.Z = cp.z;

	return xp;
}

/**
 * Constructor of 3d-point averager.
 * depth - size of points buffer.
 */
Point3dMovingAverage::Point3dMovingAverage (uint depth) : depth (depth)
{
	buf = new float [depth * 3];

	reset();
}

Point3dMovingAverage::~Point3dMovingAverage ()
{
	delete [] buf;
}

/**
 * Add point to average buffer.
 */
void Point3dMovingAverage::operator << (const cv::Point3f& pt)
{
	isCalculated = false;

	buf [ptr * 3 + 0] = pt.x;
	buf [ptr * 3 + 1] = pt.y;
	buf [ptr * 3 + 2] = pt.z;

	if (++ptr >= depth)
		ptr = 0;

	lastAdded = pt;
}

/**
 * Calculate average for points buffer.
 */
cv::Point3f& Point3dMovingAverage::point()
{
	if (isCalculated)
		return calculatedAverage;

	int n[3] = { 0,0,0 };
	float s[3] = { 0,0,0 };

	for (int i = 0; i < depth; i ++)
		for (int c = 0; c < 3; c ++) {
			float num = buf [i * 3 + c];
			if (isnormal(num)) {
				s[c] += num;
				n[c] ++;
			}
		}
	
	calculatedAverage.x = (n[0] == 0) ? 0 : s[0] / n[0];
	calculatedAverage.y = (n[1] == 0) ? 0 : s[1] / n[1];
	calculatedAverage.z = (n[2] == 0) ? 0 : s[2] / n[2];

	isCalculated = true;

	return calculatedAverage;
}

/**
 * Returns point last added to buffer.
 */
cv::Point3f& Point3dMovingAverage::lastAddedPoint ()
{
	return lastAdded;
}

cv::Point3f Point3dMovingAverage::getLastDiff ()
{
	cv::Point3f prev, diff;
	uint prev_ptr;
	if (ptr == 0)
		prev_ptr = depth - 2;
	else
	if (ptr == 1)
		prev_ptr = depth - 1;
	else
		prev_ptr = ptr - 2;


	prev.x = buf [prev_ptr * 3 + 0];
	prev.y = buf [prev_ptr * 3 + 1];
	prev.z = buf [prev_ptr * 3 + 2];

	diff.x = (isnormal (lastAdded.x) && isnormal (prev.x)) ? lastAdded.x - prev.x : 0;
	diff.y = (isnormal (lastAdded.y) && isnormal (prev.y)) ? lastAdded.y - prev.y : 0;
	diff.z = (isnormal (lastAdded.z) && isnormal (prev.z)) ? lastAdded.z - prev.z : 0;
	
	return diff;
}

/**
 * Reset averager, clear buffer, etc...
 */
void Point3dMovingAverage::reset ()
{
	ptr = 0;
	isCalculated = false;

	for (int i = 0; i < depth * 3; i ++)
		buf [i] = FP_NAN;

	lastAdded.x = FP_NAN;
	lastAdded.y = FP_NAN;
	lastAdded.z = FP_NAN;
}

/**
 * Function for compate two User by depth.
 * Used in std::sort
 */
bool UserComparatorZ::operator() (User* u1, User* u2)
{
	float u1s = (float) cv::sum (u1->getMask())[0];
	float u2s = (float) cv::sum (u2->getMask())[0];

	if (u1s <= 1000 && u2s > 1000)
		return false;
	else
	if (u1s > 1000 && u2s <= 1000)
		return true;
	else
		return u1->getMeanZ() < u2->getMeanZ();
}

/**
 * Function for compate two User by activity and depth.
 * Used in std::sort
 */
bool UserComparatorActiveDepth::operator() (User* u1, User* u2)
{
	if (u1->activity == u2->activity) {
		UserComparatorZ comp;
		return comp (u1, u2);
	}
	else
		return u1->activity > u2->activity;
}

/**
 * Private default constructor of User.
 * User can created only from KinectController.
 * ctrl - pointer to Controller;
 * id - user id for OpenNI UserGenerator.
 */
User::User (KinectController* ctrl, XnUserID id) :
	depth_ (ctrl->frameSize, CV_16UC1),
	image_ (ctrl->frameSize, CV_8UC3),
	mask_ (ctrl->frameSize, CV_8UC1),
	mask16_ (ctrl->frameSize, CV_16UC1)
{
	ctrl_ = ctrl;
	id_ = id;
	activity = MIN_ACTIVITY;
	meanZframeN = 0;
	maskFrameN = 0;
	imageFrameN = 0;
	depthFrameN = 0;
}

/**
 * Returns mask for user image.
 * mask[x,y] == 1 - user, 0 - background.
 */
const cv::Mat& User::getMask ()
{
	if (maskFrameN != ctrl_->getFrameNumber()) {
		xn::SceneMetaData sceneMd;
		if (ctrl_->userGen.GetUserPixels (0, sceneMd) == XN_STATUS_OK) {
			memcpy (mask16_.data, sceneMd.Data(), ctrl_->frameSize.width * ctrl_->frameSize.height * sizeof (XnLabel));
			XnLabel* pt = (XnLabel*)mask16_.data;
			for (int i = 0; i < ctrl_->frameSize.width * ctrl_->frameSize.height; i ++, pt ++)
				if ((*pt) == id_)
					(*pt) = 1;
				else
					(*pt) = 0;
			mask16_.convertTo (mask_, CV_8UC1);
		} else
			mask_.setTo (cv::Scalar (0));

		maskFrameN = ctrl_->getFrameNumber();
	}

	return mask_;
}

/**
 * Returns depth map of user.
 * On background depth == 0
 */
const cv::Mat& User::getDepth ()
{
	if (depthFrameN != ctrl_->getFrameNumber()) {
		cv::Mat depth;
		ctrl_->getDepth (depth);
		depth.copyTo (depth_, getMask());
		depthFrameN = ctrl_->getFrameNumber();
	}
	return depth_;
}

/**
 * Returns image of user.
 * On background image == 0,0,0
 */
const cv::Mat& User::getImage ()
{
	if (imageFrameN != ctrl_->getFrameNumber()) {
		cv::Mat image;
		ctrl_->getImage (image);
		image.copyTo (image_, getMask());
		imageFrameN = ctrl_->getFrameNumber();
	}
	return image_;
}

cv::Point3f User::getPoint (SkelPoint point)
{
	if (point == SP_CHEST)
		return getChest();
	
	return maPoints_[(int)point].point();
}

cv::Point3f User::getPointRel (SkelPoint point)
{
	if (point == SP_CHEST)
		return cv::Point3f (0,0,0);

	return cv::Point3f (getChest() - maPoints_[(int)point].point());
}
	
cv::Point User::getPointProj (SkelPoint point)
{
	cv::Point3f pt = getPoint (point);
	XnPoint3D rp[1];
	XnPoint3D pp[1];

	rp[0] = cvPoint3f_to_xnPoint3d (pt);
	ctrl_->depthGen.ConvertRealWorldToProjective (1, rp, pp);

	cv::Point proj;
	proj.x = (int) pp[0].X;
	proj.y = (int) pp[0].Y;

	return proj;
}

cv::Point3f User::getDiff (SkelPoint point)
{
	if (point == SP_CHEST) {
		cv::Point3f avp (0,0,0);
		for (int i = SP_HEAD; i <= SP_TORSO; i ++)
			avp += maPoints_[i].getLastDiff();
		avp.x /= 4;
		avp.y /= 4;
		avp.z /= 4;
		return avp;
	}

	return maPoints_[point].getLastDiff();
}

/**
 * Returns chest absolute coordinates.
 */
cv::Point3f User::getChest ()
{
	/* OLD CHEST CALCULATE
	float averageZ = 0;

	for (int i = 2; i < 6; i ++)
		averageZ += maPoints_[i].point().z;
	averageZ /= 4;

	cv::Point3f cp;
	cp.x = maPoints_[2].point().x;
	cp.y = maPoints_[2].point().y;
	cp.z = averageZ;

	return cp;
	*/

	cv::Point3f avp (0,0,0);
	for (int i = SP_HEAD; i <= SP_TORSO; i ++)
		avp += maPoints_[i].point();
	
	avp.x /= SP_TORSO - SP_HEAD + 1;
	avp.y /= SP_TORSO - SP_HEAD + 1;
	avp.z /= SP_TORSO - SP_HEAD + 1;
	return avp;
}

/**
 * Returns true if joint (lh, rh, head, neck, torso, waist) is moved by threshold.
 * resx, resy, resz - flags for use corresponding axis for calculate moving vector.
 */
bool User::pointIsMoved (SkelPoint jn, bool resx, bool resy, bool resz, float threshold)
{
	if (jn == SP_CHEST)
		return false;

	float mx = 0, my = 0, mz = 0, move;
	cv::Point3f chest = getChest();
	cv::Point3f point = chest - maPoints_[jn].lastAddedPoint();
	cv::Point3f avPoint = chest - maPoints_[jn].point();

	if (resx && isnormal(point.x)) mx = fabs (avPoint.x - point.x);
	if (resy && isnormal(point.y)) my = fabs (avPoint.y - point.y);
	if (resz && isnormal(point.z)) mz = fabs (avPoint.z - point.z);

	if (mx < 1 || my < 1)
		move = mx + my;
	else
		move = sqrt (mx * mx + my * my);
	
	if (move < 1 || mz < 1)
		move = move + mz;
	else
		move = sqrt (move * move + mz * mz);

	if (move > threshold)
		return true;
	
	return false;
}

bool User::pointIsValid (SkelPoint point)
{
	cv::Point pt = getPointProj (point);
	/* -- * /
	cv::Point3f p3 = maPoints_[(int)point].lastAddedPoint();
	XnPoint3D rp[1];
	XnPoint3D pp[1];

	rp[0] = cvPoint3f_to_xnPoint3d (p3);
	ctrl_->depthGen.ConvertRealWorldToProjective (1, rp, pp);

	cv::Point pt;
	pt.x = (int) pp[0].X;
	pt.y = (int) pp[0].Y;
	/ * -- */
	
	cv::Size is = ctrl_->getDepthSize();
	
	if (pt.x < INVALID_BORDER_X ||
	    pt.x > (is.width - INVALID_BORDER_X) ||
	    pt.y < INVALID_BORDER_Y ||
	    pt.y > (is.height - INVALID_BORDER_Y))
		return false;
	
	const cv::Mat& mask = getMask();
	
	if (!mask.at<unsigned char>(pt))
		return false;
	
	return true;
}

cv::Point2f getVecAngle (cv::Point3f v1, cv::Point3f v2)
{
	cv::Point2f angle;
	
	float mulLen = sqrt (v1.x * v1.x + v1.y * v1.y) * sqrt (v2.x * v2.x + v2.y * v2.y);
	angle.x = mulLen == 0 ? : acos ((v1.x * v2.x + v1.y * v2.y) / mulLen);
	
	mulLen = sqrt (v1.z * v1.z + v1.y * v1.y) * sqrt (v2.z * v2.z + v2.y * v2.y);
	angle.y = mulLen == 0 ? : acos ((v1.y * v2.y + v1.z * v2.z) / mulLen);
	
	return angle;
}

cv::Point2f User::getAngle (SkelAngle angle)
{
	cv::Point3f p0, v0, v1;
	
	bool show = false;
	
	switch (angle) {
	case SA_L_FOREARM:
		p0 = getPoint (SP_L_ELBOW);
		v0 = p0 - getPoint (SP_L_HAND); 
		v1 = p0 - getPoint (SP_L_SHOULDER);
		show = true;
		break;
	case SA_R_FOREARM:
		p0 = getPoint (SP_R_ELBOW);
		v0 = p0 - getPoint (SP_R_HAND); 
		v1 = p0 - getPoint (SP_R_SHOULDER);
		break;
	case SA_L_SHOULDER:
		p0 = getPoint (SP_L_SHOULDER);
		v0 = p0 - getPoint (SP_L_ELBOW); 
		v1 = p0 - getPoint (SP_NECK);
		break;
	case SA_R_SHOULDER:
		p0 = getPoint (SP_R_SHOULDER);
		v0 = p0 - getPoint (SP_R_ELBOW); 
		v1 = p0 - getPoint (SP_NECK);
		break;
	case SA_L_SHOULDER_TORSO:
		p0 = getPoint (SP_NECK);
		v0 = p0 - getPoint (SP_L_SHOULDER); 
		v1 = p0 - getPoint (SP_TORSO);
		break;
	case SA_R_SHOULDER_TORSO:
		p0 = getPoint (SP_NECK);
		v0 = p0 - getPoint (SP_R_SHOULDER); 
		v1 = p0 - getPoint (SP_TORSO);
		break;
	case SA_HEAD_TORSO:
		p0 = getPoint (SP_NECK);
		v0 = p0 - getPoint (SP_HEAD); 
		v1 = p0 - getPoint (SP_TORSO);
		break;
	case SA_TORSO_ABS:
		p0 = getPoint (SP_NECK);
		v0 = cv::Point3f (0,10,0);
		v1 = p0 - getPoint (SP_TORSO);
		break;
	default:
		return cv::Point2f (0,0);
	}
	
	cv::Point2f a = getVecAngle (v0, v1);
	//if (show)
	//	cout << "ax = " << a.x << ", ay = " << a.y << endl;
	return a;
}

/**
 * Calculate mean depth of entire user.
 */
float User::getMeanZ ()
{
	if (meanZframeN != ctrl_->getFrameNumber()) {
		cv::Mat depth;
		ctrl_->getDepth (depth);
		meanZ = (float) cv::mean (depth, getMask())[0];
		meanZframeN = ctrl_->getFrameNumber();
	}
	
	return meanZ;
}

/**
 * Set user active. Used in setUsersActivityCallback callback function.
 */
void User::setActive ()
{
	/* Check for top user */
	if (activity > MAX_ACTIVITY)
		return;
	
	if (++activity > MAX_ACTIVITY)
		activity = MAX_ACTIVITY;
}

/**
 * Set user inactive. Used in setUsersActivityCallback callback function.
 */
void User::setInactive ()
{
	/* Check for top user */
	if (activity > MAX_ACTIVITY)
		return;
	
	if (--activity < MIN_ACTIVITY)
		activity = MIN_ACTIVITY;
}

void User::setTop ()
{
	for (vector<User*>::iterator ui = ctrl_->users.begin(); ui < ctrl_->users.end(); ui ++) {
		User *u = *ui;
		u->unsetTop();
	}

	activity = MAX_ACTIVITY + 1;
}

void User::unsetTop ()
{
	if (activity > MAX_ACTIVITY)
		activity = MAX_ACTIVITY;
}

bool User::isTop()
{
	return (activity > MAX_ACTIVITY);
}

/**
 * Start track user skeleton.
 */
void User::startTracking ()
{
	cout << "start tracking " << id_ << endl;

	if (ctrl_->userGen.GetSkeletonCap().LoadCalibrationDataFromFile (id_, ctrl_->calibrationFile.c_str()) != XN_STATUS_OK)
		throw "Fail to load calibration file";
	ctrl_->userGen.GetSkeletonCap().StartTracking (id_);

	for (int i = 0; i < SP_COUNT; i ++)
		maPoints_[i].reset();
}

/**
 * Stop track user skeleton.
 */
void User::stopTracking ()
{
	cout << "stop tracking " << id_ << endl;
	
	ctrl_->userLost (this, ctrl_->userLostPriv);
	ctrl_->userGen.GetSkeletonCap().StopTracking (id_);
}

/**
 * User skeleton is tracking?
 */
bool User::isTracking ()
{
	return ctrl_->userGen.GetSkeletonCap().IsTracking (id_);
}

#define EOE(__var,__p,__action)	\
	if ((__var = (__p)) != XN_STATUS_OK) { \
		cout << __action << " fail with error #" << __var << endl; \
		throw __var; \
	}

KinectController::KinectController (int maxUsers, const string& calibrationFile, const char* recordFile) :
	calibrationFile (calibrationFile),
	maxActiveUsers (maxUsers)
{
	XnStatus err;
	mirror = false;
	
	/* Init context */
	EOE (err, context.Init(), "Init context");

	if (recordFile)
		EOE (err, context.OpenFileRecording(recordFile, recordPlayer), "Open recording");
		//EOE (err, context.OpenFileRecording(recordFile), "Open recording");
	
	EOE (err, depthGen.Create (context), "Create depth generator");
	EOE (err, imageGen.Create (context), "Create image generator");
	EOE (err, userGen.Create (context), "Create user generator");

	userGen.GetSkeletonCap().SetSkeletonProfile (XN_SKEL_PROFILE_UPPER);

	xn::DepthMetaData depth_md;
	depthGen.GetMetaData (depth_md);

	/** imageSize == depthSize (?)
	xn::ImageMetaData image_md;
	image_gen.GetMetaData (image_md);
	*/

	frameSize.width = depth_md.XRes();
	frameSize.height = depth_md.YRes();

	setUsersActivity = &setUsersActivityStub;
	userLost = NULL;
	usersPresentFunc = NULL;
	usersAbsentFunc = NULL;
}

KinectController::~KinectController ()
{
	for (vector<User*>::iterator it = users.begin(); it != users.end(); it ++)
		delete *it;
}

void KinectController::startControl ()
{
	XnStatus err;
	EOE (err, context.StartGeneratingAll(), "Start generating");

	frameNumber = 1;
}

void KinectController::stopControl ()
{
	XnStatus err;
	EOE (err, context.StopGeneratingAll(), "Stop generating");
}

void KinectController::linkToRecognizers (User* user)
{
	for (vector<AbstractRecognizer*>::iterator rit = recogs.begin(); rit != recogs.end(); rit ++)
		(*rit)->addUser (user);
}

void KinectController::unlinkFromRecognizers (User* user)
{
	for (vector<AbstractRecognizer*>::iterator rit = recogs.begin(); rit != recogs.end(); rit ++)
		(*rit)->removeUser (user);
}

bool KinectController::processFrame ()
{
	XnStatus err;

	if (!depthGen.IsGenerating())
		return false;
	
	if (context.WaitOneUpdateAll(depthGen) != XN_STATUS_OK)
		return false;

	/*
	if (!depthGen.IsDataNew())
		return false;
	*/

	/*
	if (context_.WaitAllUpdateAll() != XN_STATUS_OK)
		return false;
	*/
	for (vector<User*>::iterator it = users.begin(); it != users.end(); it ++) {
		User* user = *it;

		if (user->isTracking()) {
			XnSkeletonJointPosition joint;

			const xn::SkeletonCapability& scap = userGen.GetSkeletonCap();

			scap.GetSkeletonJointPosition (user->id_, mirror ? XN_SKEL_RIGHT_HAND : XN_SKEL_LEFT_HAND, joint);
			user->maPoints_[SP_L_HAND] << xnPoint3d_to_cvPoint3f (joint.position);

			scap.GetSkeletonJointPosition (user->id_, mirror ? XN_SKEL_LEFT_HAND : XN_SKEL_RIGHT_HAND, joint);
			user->maPoints_[SP_R_HAND] << xnPoint3d_to_cvPoint3f (joint.position);

			scap.GetSkeletonJointPosition (user->id_, mirror ? XN_SKEL_RIGHT_ELBOW : XN_SKEL_LEFT_ELBOW, joint);
			user->maPoints_[SP_L_ELBOW] << xnPoint3d_to_cvPoint3f (joint.position);

			scap.GetSkeletonJointPosition (user->id_, mirror ? XN_SKEL_LEFT_ELBOW : XN_SKEL_RIGHT_ELBOW, joint);
			user->maPoints_[SP_R_ELBOW] << xnPoint3d_to_cvPoint3f (joint.position);

			scap.GetSkeletonJointPosition (user->id_, mirror ? XN_SKEL_RIGHT_SHOULDER : XN_SKEL_LEFT_SHOULDER, joint);
			user->maPoints_[SP_L_SHOULDER] << xnPoint3d_to_cvPoint3f (joint.position);

			scap.GetSkeletonJointPosition (user->id_, mirror ? XN_SKEL_LEFT_SHOULDER : XN_SKEL_RIGHT_SHOULDER, joint);
			user->maPoints_[SP_R_SHOULDER] << xnPoint3d_to_cvPoint3f (joint.position);

			scap.GetSkeletonJointPosition (user->id_, XN_SKEL_HEAD, joint);
			user->maPoints_[SP_HEAD] << xnPoint3d_to_cvPoint3f (joint.position);

			scap.GetSkeletonJointPosition (user->id_, XN_SKEL_NECK, joint);
			user->maPoints_[SP_NECK] << xnPoint3d_to_cvPoint3f (joint.position);

			scap.GetSkeletonJointPosition (user->id_, XN_SKEL_TORSO, joint);
			user->maPoints_[SP_TORSO] << xnPoint3d_to_cvPoint3f (joint.position);
		}
	}
	
	XnUInt16 nUsers = MAX_USERS;
	XnUserID usersId [MAX_USERS];
	EOE (err, userGen.GetUsers (usersId, nUsers), "Get users");

	bool usersAdded [MAX_USERS];
	for (int i = 0; i < MAX_USERS; i ++)
		usersAdded[i] = false;

	/* Temporary vector */
	vector<User*> activeUsers;

	/* Delete lost users and push exists users to 'activeUsers' */
	for (vector<User*>::iterator it = users.begin(); it != users.end(); it ++) {
		User* user = *it;

		bool userExists = false;

		int i = 0;
		for (; i < nUsers; i ++)
			if (usersId[i] == user->id_) {
				userExists = true;
				break;
			}

		if (userExists) {
			usersAdded[i] = true;
			activeUsers.push_back (user);
		}
		else {
			cout << "User #" << user->id_ << " deleted" << endl;
			unlinkFromRecognizers (user);
			if (userLost && user->isTracking ())
				userLost (user, userLostPriv);
			delete user;
		}
	}

	/* Copy pointers to 'user' */
	users = activeUsers;

	/* Add new users to 'activeUsers' */
	for (int i = 0; i < nUsers; i ++)
		if (!usersAdded[i]) {
			User* user = new User (this, usersId[i]);
			linkToRecognizers (user);
			users.push_back (user);
		}

	if (users.size() > 0) {
		/* Sort ascending 'activeUsers' by Z-coordinate of mass center */
		sort (users.begin(), users.end(), UserComparatorZ());

		/* Call activity checker (callback) */
		setUsersActivity (users);

		activeUsers = users;

		/* Sort 'activeUsers' by activity */
		sort (activeUsers.begin(), activeUsers.end(), UserComparatorActiveDepth());
					
		/* Start track new active users, and stop track inactive and extra users */
		int au = 0;
		for (int i = 0; i < activeUsers.size(); i ++)
			if ((au < maxActiveUsers) &&
				(activeUsers[i]->activity > 0) &&
				(((float) cv::sum (activeUsers[i]->getMask())[0])) > MIN_USER_AREA)
			{
				au ++;
				if (!activeUsers[i]->isTracking())
					activeUsers[i]->startTracking();
			} else { 
				if (activeUsers[i]->isTracking())
					activeUsers[i]->stopTracking();
			}
	}

	bool trackedUsersExists = false;
	for (int i = 0; i < users.size(); i ++)
		if (users[i]->isTracking()) {
			trackedUsersExists = true;
			break;
		}

	if (trackedUsersExists) {
		/* Users present callbcak */
		usersAbTimer = 0;
		if (usersPresentFunc) {
			if (usersPrTimer == usersPrTimeout)
				usersPresentFunc (usersPrPriv);
			if ((++ usersPrTimer) > (usersPrTimeout + 1))
				usersPrTimer = usersPrTimeout + 1;
		}
	}
	else
	{
		/* Users absent callbcak */
		usersPrTimer = 0;
		if (usersAbsentFunc) {
			if (usersAbTimer == usersAbTimeout)
				usersAbsentFunc (usersAbPriv);
			if ((++ usersAbTimer) > (usersAbTimeout + 1))
				usersAbTimer = usersAbTimeout + 1;
		}
	}

	for (vector<AbstractRecognizer*>::iterator rit = recogs.begin(); rit != recogs.end(); rit ++)
		(*rit)->process();
	
	frameNumber ++;

	return true;
}

const vector<User*>& KinectController::getUsers ()
{
	return users;
}

void KinectController::getImage (cv::Mat& img)
{
	img.create (frameSize, CV_8UC3);

	memcpy (img.data, imageGen.GetRGB24ImageMap(), frameSize.width * frameSize.height * sizeof (XnRGB24Pixel));
	cv::cvtColor (img, img, CV_RGB2BGR);
}

void KinectController::getDepth (cv::Mat& depth)
{
	depth.create (frameSize, CV_16UC1);

	memcpy (depth.data, depthGen.GetDepthMap(), frameSize.width * frameSize.height * sizeof (XnDepthPixel));
}

cv::Size KinectController::getDepthSize ()
{
	xn::DepthMetaData md;
	depthGen.GetMetaData (md);
	
	return cv::Size (md.XRes(), md.YRes());
}

cv::Size KinectController::getImageSize ()
{
	xn::ImageMetaData md;
	imageGen.GetMetaData (md);
	
	return cv::Size (md.XRes(), md.YRes());
}

void KinectController::setUsersActivityCallback (SetUsersActivityFunc func)
{
	setUsersActivity = func;
}

void KinectController::setUserLostCallback (UserLostFunc func, void* priv)
{
	userLost = func;
	userLostPriv = priv;
}

void KinectController::setUsersPresentCallback(UsersPresentAbsentFunc func, int timeout, void* priv)
{
	usersPresentFunc = func;
	usersPrPriv = priv;
	usersPrTimeout = timeout;
	usersPrTimer = 0;
}

void KinectController::setUsersAbsentCallback(UsersPresentAbsentFunc func, int timeout, void* priv)
{
	usersAbsentFunc = func;
	usersAbPriv = priv;
	usersAbTimeout = timeout;
	usersAbTimer = 0;
}

void KinectController::addRecognizer (AbstractRecognizer* recog)
{
	recogs.push_back (recog);
}

void TrainableZGestureRecognizer::addUser (User* user)
{
	for (vector<TrainingSet>::iterator tsi = trainings.begin();
		tsi != trainings.end(); tsi ++)
			userData.insert (UserData (user, tsi->point));
}

void TrainableZGestureRecognizer::removeUser (User* user)
{
	for (vector<TrainingSet>::iterator tsi = trainings.begin();
		tsi != trainings.end(); tsi ++)
			userData.erase (UserData (user, tsi->point));
}

void TrainableZGestureRecognizer::process ()
{
	for (set<UserData>::iterator udIt = userData.begin(); 
		udIt != userData.end(); udIt ++)
	{
		UserData* data = (UserData*)&(*udIt);
		if (!data->user->isTracking())
			continue;
		
		cv::Point3f point;
		
		bool addPoint = false;
		bool move = true;

		if (udIt->user->pointIsValid (udIt->point)) {
			point = udIt->user->getPointRel(udIt->point);
			if (vectorLength (point - udIt->prevPoint) < GM_THRESHOLD)
				move = false;
			data->prevPoint = point;
		} else
			point = data->prevPoint;

		if (data->inGesture) {
			if (move && isnormal (point.z) && point.z > gz_exit)
				addPoint = true;
			else
				if (--(data->gestCounter) <= 0) {
					data->inGesture = false;
					if (receivedCB)
						receivedCB (data->user, data->gesture, receivePriv);
					doRecognize (data);
					data->gesture.clear();
				}
		} else {
			if (move && isnormal (point.z) && point.z > gz_enter)
				addPoint = true;
			else
				data->gestCounter = GC_MIN;
		}

		if (addPoint) {
			data->inGesture = true;
			data->gestCounter = GC_MAX;
			data->gesture.push_back (point);
		}

		if (data->inGesture)
			drawKeyPointCB (data->user, data->user->getPointProj(data->point), drawKeyPointPriv);
	}
}

static vector<cv::Point3f> interpolateGesture (const vector<cv::Point3f>& gesture, int ilen)
{
	int glen = gesture.size() - 1;
	float gin;
	int gin_a;
	int gin_b;
	float di;

	vector<cv::Point3f> igesture;
	
	for (int i = 0; i < ilen; i ++) {
		gin = (float)glen / (float)ilen * (float)i;
		gin_a = floorf (gin);
		gin_b = ceilf (gin);
		di = gin - gin_a;

		cv::Point3f pa = gesture.at (gin_a);
		cv::Point3f pb = gesture.at (gin_b);

		cv::Point3f p;

		p.x = pa.x + (pb.x - pa.x) * di;
		p.y = pa.y + (pb.y - pa.y) * di;
		p.z = pa.z + (pb.z - pa.z) * di;

		igesture.push_back (p);
	}

	return igesture;
}

static float compareGestures (const vector<cv::Point3f>& g1, const vector<cv::Point3f>& g2, bool squareMean = false)
{
	float sum = 0;
	int size = g1.size();
	
	for (int i = 0; i < size; i ++) {
		float diff = vectorLength (g2[i] - g1[i]);
		sum += squareMean ? diff * diff : fabs (diff);
	}

	sum /= size;

	return squareMean ? sqrt(sum) : sum; 
}

struct CompareResult {
	string sid;
	float matchPercent;
	float error;
};

void TrainableZGestureRecognizer::doRecognize (UserData* ud)
{
	vector<CompareResult> results;

	for (vector<TrainingSet>::iterator tsi = trainings.begin(); tsi != trainings.end(); tsi ++) {
		if (ud->point != tsi->point)
			continue;

		int trainSize = tsi->gestures.size();
		vector<cv::Point3f> igest = interpolateGesture (ud->gesture, tsi->pointsCount);

		CompareResult res;
		res.sid = tsi->sid;
		res.error = 0;

		int match = 0;
		for (vector<vector<cv::Point3f> >::iterator gi = tsi->gestures.begin(); gi != tsi->gestures.end(); gi ++) {
			float error = compareGestures (*gi, igest);
			if (error < GS_ERROR_THRESHOLD) {
				res.error += error;
				match ++;
			}
		}

		if (match > 0) {
			res.matchPercent = (float)match / (float)trainSize;
			res.error /= (float)match;

			results.push_back (res);
		}
	}

	CompareResult nearest = { "", 0, FLT_MAX };

	for (vector<CompareResult>::iterator ri = results.begin(); ri != results.end(); ri ++) {
		if ((ri->matchPercent > nearest.matchPercent) ||
		    (ri->matchPercent == nearest.matchPercent && ri->error < nearest.error))
			nearest = *ri;
	}

	//cout << "ID:" << nearest.sid << " M:" << nearest.matchPercent << " E:" << nearest.error << endl;
	if (nearest.matchPercent > GS_MATCH_THRESHOLD)
		if (recognizedCB)
			recognizedCB (ud->user, nearest.sid, nearest.matchPercent, nearest.error, recognizePriv);
}

bool TrainableZGestureRecognizer::loadTrainingSet (const string& yamlfile, SkelPoint point, const string& sid)
{
	cv::FileStorage fs (yamlfile, cv::FileStorage::READ);
	if (!fs.isOpened()) {
		cout << "Fail to load " << yamlfile << endl;
		return false;
	}

	cv::FileNode root = fs.root();
	for (cv::FileNodeIterator i_ts = root.begin(); i_ts != root.end(); i_ts ++) {

		if (!(*i_ts).isSeq())
			continue;

		TrainingSet tSet;
		tSet.sid = sid;
		tSet.pointsCount = 0;
		tSet.point = point;

		for (cv::FileNodeIterator i_fs = (*i_ts).begin(); i_fs != (*i_ts).end(); i_fs ++) {
			/* Read gesture vector */
			vector<cv::Point3f> gesture;

			for (cv::FileNodeIterator i_gest = (*i_fs).begin(); i_gest != (*i_fs).end(); i_gest ++) {
				cv::Point3f p;
				(*i_gest)["x"] >> p.x;
				(*i_gest)["y"] >> p.y;
				(*i_gest)["z"] >> p.z;

				gesture.push_back (p);
			}

			if (tSet.pointsCount == 0)
				tSet.pointsCount = gesture.size();

			if (tSet.pointsCount != gesture.size()) {
				cout << "Gestures size not constant!" << endl;
				return false;
			}

			tSet.gestures.push_back (gesture);
		}

		if (tSet.pointsCount > 0)
			trainings.push_back (tSet);
	}

	return true;
}

void BorderLimitRecognizer::addUser (User* user)
{
	for (vector<OutBorderLimit>::iterator li = limits.begin();
			li != limits.end(); li ++)
		userData.insert (UserData (user, &(*li)));
}

void BorderLimitRecognizer::removeUser (User* user)
{
	for (vector<OutBorderLimit>::iterator li = limits.begin();
			li != limits.end(); li ++)
		userData.erase (UserData (user, &(*li)));
}

void BorderLimitRecognizer::process ()
{
	for (set<UserData>::iterator udIt = userData.begin();
		udIt != userData.end(); udIt ++)
	{
		UserData* data = (UserData*)&(*udIt);
		if (!data->user->isTracking())
			continue;

		const OutBorderLimit* limit = udIt->limit;
		cv::Point3f point = data->user->getPointRel (limit->point);
		
		bool inLimit = false;
		
		if (data->user->pointIsValid (limit->point)) {
			switch (limit->limit) {
			/*
			case LIMIT_UP:		inLimit = (limit->threshold < -point.y); break;
			case LIMIT_DOWN:	inLimit = (limit->threshold < point.y); break;
			case LIMIT_LEFT:	inLimit = (limit->threshold < -point.x); break;
			case LIMIT_RIGHT:	inLimit = (limit->threshold < point.x); break;
			case LIMIT_FORWARD:	inLimit = (limit->threshold < point.z); break;
			*/
			case LIMIT_UP:		inLimit = ((-point.y >= limit->down) && (-point.y <= limit->up)); break;
			case LIMIT_DOWN:	inLimit = ((point.y >= limit->down) && (point.y <= limit->up)); break;
			case LIMIT_LEFT:	inLimit = ((-point.x >= limit->down) && (-point.x <= limit->up)); break;
			case LIMIT_RIGHT:	inLimit = ((point.x >= limit->down) && (point.x <= limit->up)); break;
			case LIMIT_FORWARD:	inLimit = ((point.z >= limit->down) && (point.z <= limit->up)); break;
			}
		}

		bool isMoved = 
			limit->nostop ? false :
				data->user->pointIsMoved (limit->point, true, true, true, BorderLimitRecognizer::PM_THRESHOLD);
		
		if (inLimit) {
			if (data->entered) {
				data->timer = limit->timeout;
			} else {
				if (!isMoved && (data->timer < limit->timeout)) {
					if (++data->timer >= limit->timeout) {
						data->entered = true;
						data->timer = limit->timeout;
						inLimitCB (data->user, limit->sid, true, inLimitCBPriv);
					}
					drawKeyPointCB (data->user, data->user->getPointProj(limit->point), drawKeyPointPriv);
				} else
					data->timer = 0;
			}
		}
		else
		{
			if (!data->entered) {
				data->timer = 0;
			} else {
				if (data->timer > 0) {
					if (--data->timer <= 0) {
						data->timer = 0;
						data->entered = false;
						inLimitCB (data->user, limit->sid, false, inLimitCBPriv);
					}
				}
			}
		}
	}
}

void BorderLimitRecognizer::addLimit (int limit, float down, float up, int timeout, bool nostop, SkelPoint point, const string& sid)
{
	if (up == 0)
		up = INF_LIMIT;
	limits.push_back (OutBorderLimit (limit, down, up, timeout, nostop, point, sid));
}

void TrainablePoseRecognizer::addUser (User* user)
{
	for (vector<TrainingSet>::iterator ti = trainings.begin();
			ti != trainings.end(); ti ++)
		userData.insert (UserData (user, &(*ti)));
}
       
void TrainablePoseRecognizer::removeUser (User* user)
{
	for (vector<TrainingSet>::iterator ti = trainings.begin();
			ti != trainings.end(); ti ++)
		userData.erase (UserData (user, &(*ti)));
}
        
void TrainablePoseRecognizer::process ()
{
	for (set<UserData>::iterator udIt = userData.begin();
		udIt != userData.end(); udIt ++)
	{
		UserData* data = (UserData*)&(*udIt);
		if (!data->user->isTracking())
			continue;
	
		/* ---- Common pose detect ---- */
		cv::Point2f angleDiff[SA_COUNT];

		bool fixPose = true;
		for (int i = 0; i < SA_COUNT; i ++) {
			cv::Point2f a = data->user->getAngle((SkelAngle)i);
			
			angleDiff[i].x = fabs (a.x - data->prevAngle[i].x);
			angleDiff[i].y = fabs (a.y - data->prevAngle[i].y);
			data->prevAngle[i] = a;
			
			if (angleDiff[i].x > cp_angleTH ||
			    angleDiff[i].y > cp_angleTH)
				fixPose = false;
		}
		
		if (fixPose) {
			if (data->cp_timer < cp_poseTO) {
				data->cp_timer += 1;
				if (data->cp_timer == cp_poseTO)
					if (poseDetectCB)
						poseDetectCB (data->user, poseDetectCBPriv);
			}
		} else {
			if (data->cp_timer == cp_poseTO)
				data->cp_timer = 0;
			else
				if (data->cp_timer > 0)
					data->cp_timer -= 1;
		}
		
		/* ---- Pose recognize ---- */
		fixPose = (data->pose->pose_av.size () == 0) ? false : true;
		
		//cout << "diff: ";
		for (int i = 0; i < data->pose->pose_av.size (); i ++) {
			const pair<SkelAngle, cv::Point2f>& av = data->pose->pose_av.at (i);
			const pair<SkelAngle, cv::Point2f>& re = data->pose->pose_re.at (i);
			
			cv::Point2f a = data->user->getAngle(av.first);
			cv::Point2f diff = a - av.second;
			
		//	cout << diff.x << "\t" << diff.y << "\t";
			
			if (fabs(diff.x) > re.second.x ||
			    fabs(diff.y) > re.second.y) {
				fixPose = false;
				break;
			}
		}
		//cout << endl;
		
#define POSE_TIMEOUT	15
		
		if (fixPose) {
			if (data->timer < POSE_TIMEOUT) {
				data->timer += 1;
				if (data->timer == POSE_TIMEOUT)
					if (poseRecogCB)
						poseRecogCB (data->user, data->pose->sid, poseRecogCBPriv);
			}
		} else {
			if (data->timer == POSE_TIMEOUT)
				data->timer = 0;
			else
				if (data->timer > 0)
					data->timer -= 1;
		}
		
	}
}

bool TrainablePoseRecognizer::loadTrainingSet (const std::string& yamlfile, const std::string& sid)
{
	return false;
}

bool TrainablePoseRecognizer::loadTrainingSet (std::vector<std::pair<SkelAngle, cv::Point2f> > pose_av, std::vector<std::pair<SkelAngle, cv::Point2f> > pose_re, const std::string& sid)
{
	TrainingSet ts;
	ts.pose_av = pose_av;
	ts.pose_re = pose_re;
	ts.sid = sid;
	trainings.push_back (ts);
	
	return true;
}

static cv::Point3f pointAbs (cv::Point3f p)
{
	cv::Point3f abs;
	abs.x = fabs (p.x);
	abs.y = fabs (p.y);
	abs.z = fabs (p.z);
	
	return abs;
}

void HandUpRecognizer::process ()
{
	for (set<UserData>::iterator udIt = userData.begin();
		udIt != userData.end(); udIt ++)
	{
		UserData* data = (UserData*)&(*udIt);
		User* user = data->user;
		if (!user->isTracking())
			continue;
		
		cv::Point3f ld = user->getPoint (SP_L_HAND) - user->getPoint (SP_L_ELBOW);
		cv::Point3f rd = user->getPoint (SP_R_HAND) - user->getPoint (SP_R_ELBOW);
		cv::Point3f lda = pointAbs (ld);
		cv::Point3f rda = pointAbs (rd);
		float lfal = vectorLength (ld);
		float rfal = vectorLength (rd);
		
		/* ---- LEFT HAND ---- */
		if (lda.x <= MAXDIFF && lda.z <= MAXDIFF && lfal <= MAX_FAL && lfal >= MIN_FAL && ld.y > 0) {
			if (drawKeyPointCB) {
				drawKeyPointCB (user, user->getPointProj (SP_L_HAND), NULL);
				drawKeyPointCB (user, user->getPointProj (SP_L_ELBOW), NULL);
			}
			
			if (data->ltimer < timeout) {
				data->ltimer += 1;
				if (data->ltimer == timeout)
					if (handIsUpCB) {
						cout << "lfal = " << lfal << endl;
						handIsUpCB (data->user, true, handIsUpCBPriv);
					}
			}
		}
		else 
		{
			if (data->ltimer == timeout)
				data->ltimer = 0;
			else
				if (data->ltimer > 0)
					data->ltimer -= 1;
		}
		
		/* ---- RIGHT HAND ---- */
		if (rda.x <= MAXDIFF && rda.z <= MAXDIFF && rfal <= MAX_FAL && rfal >= MIN_FAL && rd.y > 0) {
			if (drawKeyPointCB) {
				drawKeyPointCB (user, user->getPointProj (SP_R_HAND), NULL);
				drawKeyPointCB (user, user->getPointProj (SP_R_ELBOW), NULL);
			}
			
			if (data->rtimer < timeout) {
				data->rtimer += 1;
				if (data->rtimer == timeout) {
					cout << "rfal = " << rfal << endl;
					if (handIsUpCB)
						handIsUpCB (data->user, false, handIsUpCBPriv);
				}
			}
		}
		else
		{
			if (data->rtimer == timeout)
				data->rtimer = 0;
			else
				if (data->rtimer > 0)
					data->rtimer -= 1;
		}
	}
}

} /* namespace em */
