#ifndef SKELETON_HPP
#define SKELETON_HPP 

#include <iostream>
#include <vector>
#include <set>
#include <XnCppWrapper.h>
#include <opencv2/opencv.hpp>

namespace em {

class Point3dMovingAverage {
protected:
	uint depth;
	float* buf;
	uint ptr;
	bool isCalculated;
	cv::Point3f calculatedAverage;
	cv::Point3f lastAdded;
public:
	Point3dMovingAverage (uint depth = 3);
	~Point3dMovingAverage ();

	void operator << (const cv::Point3f& pt);
	cv::Point3f& point();
	operator cv::Point3f&() { return point(); }

	cv::Point3f& lastAddedPoint ();
	cv::Point3f getLastDiff ();
	void reset();
};

class User;
class AbstractRecognizer;

typedef void (*SetUsersActivityFunc)(std::vector<User*>);
typedef void (*UserLostFunc)(User* user, void* priv);
typedef void (*UsersPresentAbsentFunc)(void* priv);

class KinectController {
protected:
	unsigned long frameNumber;
	bool mirror;

	xn::Context context;
	xn::DepthGenerator depthGen;
	xn::ImageGenerator imageGen;
	xn::UserGenerator userGen;
	xn::Player recordPlayer;

	cv::Size frameSize;

	std::vector<User*> users;
	int maxActiveUsers;
	std::string calibrationFile;

	SetUsersActivityFunc setUsersActivity;
	UserLostFunc userLost;
	void* userLostPriv;
	
	UsersPresentAbsentFunc usersPresentFunc;
	void* usersPrPriv;
	int usersPrTimeout;
	int usersPrTimer;
	
	UsersPresentAbsentFunc usersAbsentFunc;
	void* usersAbPriv;
	int usersAbTimeout;
	int usersAbTimer;

	std::vector<AbstractRecognizer*> recogs;
	void linkToRecognizers (User* user);
	void unlinkFromRecognizers (User* user);

public:
	friend class User;

	static const int MAX_USERS = 16;
	static const int MIN_USER_AREA = 2000; // 10000

	KinectController (int maxActiveUsers, const std::string& calibrationFile, const char* recordFile = NULL);
	/* TODO delete recognizers ? */
	~KinectController ();
	void startControl ();
	void stopControl ();

	bool processFrame ();
	unsigned long getFrameNumber () { return frameNumber; };

	const std::vector<User*>& getUsers ();

	void getImage (cv::Mat& image);
	void getDepth (cv::Mat& depth);
	
	cv::Size getDepthSize ();
	cv::Size getImageSize ();

	void setUsersActivityCallback (SetUsersActivityFunc func);
	void setUserLostCallback (UserLostFunc func, void* priv);
	void setUsersPresentCallback (UsersPresentAbsentFunc func, int timeout, void* priv);
	void setUsersAbsentCallback (UsersPresentAbsentFunc func, int timeout, void* priv);

	void addRecognizer (AbstractRecognizer* recog);
	
	void setMirror (bool mirror) {
		this->mirror = mirror;
	}
	
	bool getMirror () {
		return mirror;
	}
};

struct UserComparatorZ {
	bool operator() (User* u1, User* u2);
};

struct UserComparatorActiveDepth
{
	bool operator() (User* u1, User* u2);
};

enum SkelPoint {
	SP_L_HAND = 0,
	SP_R_HAND,
	SP_L_ELBOW,
	SP_R_ELBOW,
	SP_L_SHOULDER,
	SP_R_SHOULDER,
	SP_HEAD,
	SP_NECK,
	SP_TORSO,
	SP_CHEST,
	SP_COUNT
};

enum SkelAngle {
	SA_L_FOREARM = 0,
	SA_R_FOREARM,
	SA_L_SHOULDER,
	SA_R_SHOULDER,
	SA_L_SHOULDER_TORSO,
	SA_R_SHOULDER_TORSO,
	SA_HEAD_TORSO,
	SA_TORSO_ABS,
	SA_COUNT
};

class User {
protected:
	User (KinectController* ctrl, XnUserID id);
	
	XnUserID id_;
	KinectController* ctrl_;

	int activity;
	float meanZ;
	cv::Mat depth_;
	cv::Mat image_;
	cv::Mat mask_;
	cv::Mat mask16_;

	Point3dMovingAverage maPoints_[SP_COUNT];
	//cv::Point3f lastValidPoints_[6];

	/* 'calculated' flags */
	unsigned long meanZframeN;
	unsigned long maskFrameN;
	unsigned long imageFrameN;
	unsigned long depthFrameN;

	bool jointIsMoved (int jn, bool resx, bool resy, bool resz, float threshold);
	cv::Point3f getChest ();

	void startTracking ();
	void stopTracking ();
public:
	friend class KinectController;
	friend struct UserComparatorZ;
	friend struct UserComparatorActiveDepth;

	static const int MAX_ACTIVITY = 10;
	static const int MIN_ACTIVITY = -10;
	static const int INVALID_BORDER_X = 10;
	static const int INVALID_BORDER_Y = 10;

	cv::Point3f	getPoint (SkelPoint point);
	cv::Point3f	getPointRel (SkelPoint point);
	cv::Point	getPointProj (SkelPoint point);
	cv::Point3f	getDiff (SkelPoint point);
	bool		pointIsMoved (SkelPoint point, bool resx, bool resy, bool resz, float threshold);

	bool		pointIsValid (SkelPoint point);
	
	cv::Point2f	getAngle (SkelAngle angle);
	
	const cv::Mat& getMask ();
	const cv::Mat& getDepth ();
	const cv::Mat& getImage ();

	float getMeanZ ();

	void setActive ();
	void setInactive ();
	
	void setTop ();
	void unsetTop();
	bool isTop();

	bool isTracking ();
};

typedef void (*ArDrawKeyPointCB)(User* user, const cv::Point& point, void* priv);

/* Interface for all recognizers (gestures, poses etc.) */
class AbstractRecognizer {
protected:
        std::set<User*> users;
    
	ArDrawKeyPointCB drawKeyPointCB;
	void* drawKeyPointPriv;
public:
	AbstractRecognizer () : drawKeyPointCB(NULL) {}
        
        virtual void addUser (User* user) = 0;
        virtual void removeUser (User* user) = 0;
        virtual void process () = 0;
        
	void setDrawKeyPointCB (ArDrawKeyPointCB cb, void* priv) {
		drawKeyPointCB = cb;
		drawKeyPointPriv = priv;
	}
};

typedef void (*GestureReceivedCB)(User* user, const std::vector<cv::Point3f>&, void* priv);
typedef void (*GestureRecognizedCB)(User* user, const std::string& sid, float likelihood, float error, void* priv);

class TrainableZGestureRecognizer : public AbstractRecognizer {
protected:
	/* Training set for trainable gesture recognizer */
	struct TrainingSet {
		std::vector<std::vector<cv::Point3f> > gestures;
		std::string sid;
		int pointsCount;
		SkelPoint point;
	};

	/* Gesture data for each user */
	struct UserData {
		UserData (User* user, SkelPoint point) : 
			user (user), point (point), prevPoint (0,0,0),
			inGesture (false), gestCounter (GC_MIN) {}
		
		bool operator < (const UserData& ud) const {
			if (user == ud.user)
				return (point < ud.point);
			else
				return (user < ud.user);
		}
		
		User* user;
		SkelPoint point;
		
		std::vector<cv::Point3f> gesture;
		cv::Point3f prevPoint;
		bool inGesture;
		int gestCounter;
	};

	std::vector<TrainingSet> trainings;
	std::set<UserData> userData;
	
	static const int GC_MIN = -5;
	static const int GC_MAX = 5;
	static const int GM_THRESHOLD = 10;
	static const float GS_ERROR_THRESHOLD = 200.0;
	static const float GS_MATCH_THRESHOLD = 0.6;

        /* Callbacks */
        GestureReceivedCB receivedCB;
	GestureRecognizedCB recognizedCB;
	
        /* Callbacks private data */
	void* receivePriv;
	void* recognizePriv;

	int gz_enter, gz_exit;

	void doRecognize (UserData* ud);
public:
	static const int GZ_ENTER = 350;
	static const int GZ_EXIT = 350;

	TrainableZGestureRecognizer (int gz_enter = GZ_ENTER, int gz_exit = GZ_EXIT) : 
		AbstractRecognizer (), receivedCB (NULL), recognizedCB (NULL),                
                gz_enter (gz_enter), gz_exit (gz_exit) {};

	void addUser (User* user);
        void removeUser (User* user);
        void process ();
	
	bool loadTrainingSet (const std::string& yamlfile, SkelPoint point, const std::string& sid);

        void setGestureReceivedCB (GestureReceivedCB cb, void* priv) {
		receivedCB = cb;
		receivePriv = priv;
	}

	void setGestureRecognizedCB (GestureRecognizedCB cb, void* priv) {
		recognizedCB = cb;
		recognizePriv = priv;
	}
};


typedef void (*OutBorderInLimitCB)(User* user, const std::string& sid, bool inLimit, void* priv);

/* Border limit recognizer */
class BorderLimitRecognizer : public AbstractRecognizer {
protected:
	/* Border limit description for BorderLimitRecognizer */
	struct OutBorderLimit {
		int limit;
		float up;
		float down;
		int timeout;
		bool nostop;
		std::string sid;
		SkelPoint point;

		OutBorderLimit (int limit, float down, float up, int timeout, bool nostop, SkelPoint point, const std::string& sid) :
			limit (limit), down (down), up (up), timeout (timeout), sid (sid), point (point), nostop (nostop) {}
	};

	struct UserData {
		User* user;
		const OutBorderLimit* limit;
		int timer;
		bool entered;

		UserData (User* user, const OutBorderLimit* limit) : 
			user (user), limit (limit), timer (0), entered (false) {}
		
		bool operator < (const UserData& ud) const {
			if (user == ud.user)
				return (limit < ud.limit);
			else
				return (user < ud.user);
		}
	};
	
	static const int PM_THRESHOLD = 12;

	std::vector<OutBorderLimit> limits;
	std::set<UserData> userData;

	OutBorderInLimitCB inLimitCB;
	void* inLimitCBPriv;
public:
	static const int LIMIT_UP = 0;
	static const int LIMIT_DOWN = 1;
	static const int LIMIT_LEFT = 2;
	static const int LIMIT_RIGHT = 3;
	static const int LIMIT_FORWARD = 4;
	
	static const float INF_LIMIT = 1000000;

	BorderLimitRecognizer () : AbstractRecognizer () {}
	
	void addUser (User* user);
        void removeUser (User* user);
	void process ();
	
	void addLimit (int limit, float down, float up, int timeout, bool nostop, SkelPoint point, const std::string& sid);

	void setInLimitCB (OutBorderInLimitCB cb, void* priv) {
		inLimitCB = cb;
		inLimitCBPriv = priv;
	}
};


typedef void (*PoseDetectCB)(User* user, void* priv);
typedef void (*PoseRecognizedCB)(User* user, const std::string& sid, void* priv);

class TrainablePoseRecognizer : public AbstractRecognizer {
protected:
	/* Training set for trainable gesture recognizer */
	struct TrainingSet {
		std::vector<std::pair<SkelAngle, cv::Point2f> > pose_av;
		std::vector<std::pair<SkelAngle, cv::Point2f> > pose_re;
		std::string sid;
	};
	
	struct UserData {
		UserData (User* user, TrainingSet* pose) : user (user), pose (pose), timer (0), cp_timer (0) {}
		
		bool operator < (const UserData& ud) const {
			if (user == ud.user)
				return (pose < ud.pose);
			else
				return (user < ud.user);
		}

		
		User* user;
		const TrainingSet* pose;

		mutable cv::Point2f prevAngle[SA_COUNT];
		mutable int timer;
		mutable int cp_timer;
	};

	std::vector<TrainingSet> trainings;
	std::set<UserData> userData;
	
	PoseDetectCB poseDetectCB;
	PoseRecognizedCB poseRecogCB;
	void* poseDetectCBPriv;
	void* poseRecogCBPriv;
	
	int cp_poseTO;
	float cp_angleTH;
	
	//const std::string recognize ()
public:
	static const int POSE_TIMEOUT = 30;
	static const float POINT_DIVER = 10;
	
	TrainablePoseRecognizer () : AbstractRecognizer() {}
	
	void addUser (User* user);
        void removeUser (User* user);
        void process ();

	bool loadTrainingSet (const std::string& yamlfile, const std::string& sid);
	bool loadTrainingSet (std::vector<std::pair<SkelAngle, cv::Point2f> > pose_av, std::vector<std::pair<SkelAngle, cv::Point2f> > pose_re, const std::string& sid);
	void addFakeTrainingSet () {
		trainings.push_back(TrainingSet());
	}

	void setPoseDetectCB (PoseDetectCB cb, int poseTimeout, float angleThreshold, void* priv) {
		poseDetectCB = cb;
		poseDetectCBPriv = priv;
		cp_poseTO = poseTimeout;
		cp_angleTH = angleThreshold;
	}

	void setPoseRecognizedCB (PoseRecognizedCB cb, void* priv) {
		poseRecogCB = cb;
		poseRecogCBPriv = priv;
	}
};
 

typedef void (*HandIsUpCB)(User* user, bool left, void* priv);

class HandUpRecognizer : public AbstractRecognizer {
protected:
	struct UserData {
		UserData (User* user) : user (user), ltimer (0), rtimer (0) {}
		
		bool operator < (const UserData& ud) const {
			return (user < ud.user);
		}

		User* user;

		mutable int ltimer;
		mutable int rtimer;
	};

	std::set<UserData> userData;
	int timeout;
	
	HandIsUpCB handIsUpCB;
	void* handIsUpCBPriv;
public:
	static const int TIMEOUT = 30;
	static const int MAXDIFF = 100;
	static const int MAX_FAL = 420;
	static const int MIN_FAL = 220;
	
	HandUpRecognizer (int timeout = TIMEOUT) : AbstractRecognizer(), timeout (timeout) {}
	
	void addUser (User* user) {
		userData.insert (UserData(user));
	}

	void removeUser (User* user) {
		userData.erase (UserData(user));
	}
	
        void process ();

	void setTimeout (int timeout) {
		this->timeout = timeout;
	}
	
	void setHandIsUpCB (HandIsUpCB cb, void* priv) {
		handIsUpCB = cb;
		handIsUpCBPriv = priv;
	}
};

} /* namespace em; */

#endif /* SKELETON_HPP */
