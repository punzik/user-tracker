#include <iostream>

#include "../config.hpp"

using namespace std;

int main (int argc, char** argv)
{
	if (argc < 2) {
		cout << "t-config <config.cfg>" << endl;
		return -1;
	}

	em::Config cfg (argv[1]);

	cout << cfg.size() << endl;
	cfg.debugPrint();

	try {

	cout << cfg["system"]["data-path"][1].toString() << endl;
	cout << cfg["system"]["ev-server-port"][1].toInt() << endl;

	} catch (em::ConfigException ex) {
		cout << ex.message << endl;
	}

	return 0;
}
