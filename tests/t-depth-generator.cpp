#include <iostream>

#include <XnCppWrapper.h>

#include <opencv2/opencv.hpp>

using namespace std;

#define EXIT_ON_ERROR(__var,__p,__action)	\
	if ((__var = __p) != XN_STATUS_OK) { \
		cout << __action << " fail with error #" << __var << endl; \
		return -1; \
	}

int
main (int argc, char** argv)
{
	XnStatus err;
	xn::Context xn_context;

	EXIT_ON_ERROR (err, xn_context.Init(), "Init context");
	EXIT_ON_ERROR (err, xn_context.OpenFileRecording(argv[1]), "Open recording");

/*
	xn::EnumerationErrors errors;
	EXIT_ON_ERROR (err, xn_context.InitFromXmlFile("/home/np/projects/openni-nite/user-tracker/__build/SamplesConfig.xml", &errors), "Init context");
*/

	xn::DepthGenerator depth;
	EXIT_ON_ERROR (err, depth.Create (xn_context), "Create depth generator");

	xn::DepthMetaData depth_md;
	depth.GetMetaData (depth_md);
	cout << "Depth width=" << depth_md.XRes() << ", height=" << depth_md.YRes() << endl;

	cv::Size depth_size (depth_md.XRes(), depth_md.YRes());
	cv::Mat depth_mat (depth_size, CV_16UC1);
	cv::Mat depth_8u;

	EXIT_ON_ERROR (err, xn_context.StartGeneratingAll(), "Start generating");

	while (true) {
		EXIT_ON_ERROR (err, xn_context.WaitOneUpdateAll(depth), "Waiting");
		
		memcpy (depth_mat.data, depth.GetDepthMap(), depth_size.width * depth_size.height * sizeof (XnDepthPixel));
		depth_mat.convertTo (depth_8u, CV_8UC1, 1./32.);
		cv::imshow ("depth", depth_8u);
		
		cv::waitKey (5);
	}

	return 0;
}
