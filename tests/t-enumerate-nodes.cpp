#include <XnOpenNI.h>
#include <XnCppWrapper.h>

#include <iostream>

using namespace std;

int
main (int argc, char** argv)
{
	XnStatus err;
	xn::Context xn_context;

	if ((err = xn_context.Init()) != XN_STATUS_OK) {
		cout << "Init context fail with error #" << err << endl;
		return -1;
	}

	if ((err = xn_context.OpenFileRecording(argv[1])) != XN_STATUS_OK) {
		cout << "Open recording fail with error #" << err << endl;
		return -1;
	}

/*
	xn::EnumerationErrors errors;
	if ((err = xn_context.InitFromXmlFile("/home/np/projects/openni-nite/user-tracker/__build/SamplesConfig.xml", &errors)) != XN_STATUS_OK) {
		cout << "Init context fail with error #" << err << endl;
		return -1;
	}
*/

	xn::NodeInfoList node_list;

	if ((err = xn_context.EnumerateExistingNodes (node_list)) != XN_STATUS_OK) {
		cout << "Enumerate nodes fail with error #" << err << endl;
		return -1;
	}

	xn::NodeInfoList::Iterator it = node_list.Begin();
	for (; it != node_list.End(); it ++) {
		xn::NodeInfo node_info = *it;
		XnProductionNodeDescription nd = node_info.GetDescription();
		cout << "Vendor:   " << nd.strVendor << endl;
		cout << "Name:     " << nd.strName << endl;
		cout << "Version:  " << (int)nd.Version.nMajor << "." << (int)nd.Version.nMinor << endl;
		cout << "Instance: " << node_info.GetInstanceName() << endl;
		cout << endl;
	}

	return 0;
}
