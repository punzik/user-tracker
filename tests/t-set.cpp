#include <cstdlib>
#include <iostream>
#include <set>

using namespace std;

class MyClass
{
public:
	int n;
	
	MyClass (int n) : n (n) {}
	bool operator < (const MyClass& c) const {
		return (n < c.n);
	}
};


int main(int argc, char** argv) {

	set<MyClass> mySet;
	
	MyClass c1 (1);
	
	mySet.insert (MyClass(c1));
	mySet.insert (MyClass(2));
	mySet.insert (MyClass(1));
	mySet.insert (MyClass(5));
	mySet.insert (MyClass(7));
	mySet.insert (MyClass(3));
	mySet.insert (MyClass(2));
	mySet.insert (MyClass(5));
	  
	for (set<MyClass>::iterator it = mySet.begin(); it != mySet.end(); it ++)
		cout << it->n << endl;
	
	return 0;
}

