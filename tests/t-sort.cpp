#include <opencv2/opencv.hpp>
#include <iostream>

int
main (int argc, char** argv)
{
	cv::Mat mat (10, 1, CV_32FC1);
	cv::Mat idx (10, 1, CV_16SC1);

	mat.at<float>(0) = 3;
	mat.at<float>(1) = 4;
	mat.at<float>(2) = 1;
	mat.at<float>(3) = 0;
	mat.at<float>(4) = 7;
	mat.at<float>(5) = 5;
	mat.at<float>(6) = 8;
	mat.at<float>(7) = 2;
	mat.at<float>(8) = 9;
	mat.at<float>(9) = 6;

	cv::sortIdx (mat, idx, CV_SORT_EVERY_COLUMN | CV_SORT_ASCENDING);

	for (int i = 0; i < 10; i ++) {
		int n = idx.at<short>(i);
		std::cout << "mat[" << n << "] = " << mat.at<float>(n) << std::endl;
	}

	return 0;
}
