#include <sstream>
#include <iostream>
#include <string>

using namespace std;

class StringTokenizer {
private:
	string str;
	int pos;
public:
	StringTokenizer (string str) : str (str), pos (0) {}
	bool nextToken (string& t);
};

bool StringTokenizer::nextToken (string& t)
{
	bool end = false;
	bool begin = false;
	bool dquote = false;

	if (pos >= str.size())
		return false;

	ostringstream st;

	while (!end) {
		char c = str[pos];
		
		switch (c) {
			case '"':
				dquote = !dquote;
				begin = true;
				break;

			case ';':
				if (!dquote) {
					pos = str.size() - 1;
					end = true;
					break;
				}
			case ' ':
			case '\t':
				if (dquote) {
					st.put (c);
					break;
				}
				if (!begin)
					break;
			case '\n':
			case '\r':
			case '\0':
				end = true;
				break;
			default:
				st.put (c);
				begin = true;
		}

		if (++pos >= str.size())
			end = true;
	}

	string s = st.str();
	if (s.size() == 0)
		return false;

	t = s;

	return true;
}

int main (void)
{
	string s = "  Hello\t\t\"  from old  \"   shoes! ; hahaha! \n";
	string t;

	StringTokenizer st (s);

	while (st.nextToken(t))
		cout << "#" << t << "#" << endl;

	return 0;
}
