#include <iostream>
#include <math.h>
#include <sys/time.h>

#include <XnCppWrapper.h>

#include <opencv2/opencv.hpp>

using namespace std;

#define EOE(__var,__p,__action)	\
	if ((__var = __p) != XN_STATUS_OK) { \
		cout << __action << " fail with error #" << __var << endl; \
		return -1; \
	}

class XnPoint3DMovingAverage {
protected:
	uint depth;
	float *buf;
	uint ptr;
	bool isCalculated;
	XnPoint3D calculatedAverage;
public:
	XnPoint3DMovingAverage (uint depth = 3);
	~XnPoint3DMovingAverage ();

	void operator << (const XnPoint3D& pt);
	XnPoint3D& point();
	operator XnPoint3D&() { return point(); }
};

XnPoint3DMovingAverage::XnPoint3DMovingAverage (uint depth) : depth (depth)
{
	ptr = 0;
	isCalculated = false;
	buf = new float [depth * 3];

	for (int i = 0; i < depth * 3; i ++)
		buf [i] = FP_NAN;
}

XnPoint3DMovingAverage::~XnPoint3DMovingAverage ()
{
	delete buf;
}

void XnPoint3DMovingAverage::operator << (const XnPoint3D& pt)
{
	isCalculated = false;

	buf [ptr * 3 + 0] = pt.X;
	buf [ptr * 3 + 1] = pt.Y;
	buf [ptr * 3 + 2] = pt.Z;

	if (++ptr >= depth)
		ptr = 0;
}

XnPoint3D& XnPoint3DMovingAverage::point()
{
	if (isCalculated)
		return calculatedAverage;

	int n[3] = { 0,0,0 };
	float s[3] = { 0,0,0 };

	for (int i = 0; i < depth; i ++)
		for (int c = 0; c < 3; c ++) {
			float num = buf [i * 3 + c];
			if (isnormal(num)) {
				s[c] += num;
				n[c] ++;
			}
		}
	
	calculatedAverage.X = n[0] == 0 ? 0 : s[0] / n[0];
	calculatedAverage.Y = n[1] == 0 ? 0 : s[1] / n[1];
	calculatedAverage.Z = n[2] == 0 ? 0 : s[2] / n[2];

	isCalculated = true;

	return calculatedAverage;
}

bool isMoves (XnPoint3D point, XnPoint3D average, bool resx, bool resy, bool resz, float moveThreshold)
{
	float mx = 0, my = 0, mz = 0, move;

	if (resx && isnormal(point.X)) mz = fabs (average.X - point.X);
	if (resy && isnormal(point.Y)) my = fabs (average.Y - point.Y);
	if (resz && isnormal(point.Z)) mz = fabs (average.Z - point.Z);

	if (mx < 1 || my < 1)
		move = mx + my;
	else
		move = sqrt (mx * mx + my * my);
	
	if (move < 1 || mz < 1)
		move = move + mz;
	else
		move = sqrt (move * move + mz * mz);

	if (move > moveThreshold)
		return true;
	
	return false;
}

void
detect_user (xn::UserGenerator &generator, XnUserID user, void *pCookie)
{
	cout << "User #" << user << " detected" << endl;

	xn::UserGenerator* user_gen = (xn::UserGenerator*) pCookie;
	xn::SkeletonCapability skeleton_cap = user_gen->GetSkeletonCap();
	skeleton_cap.LoadCalibrationDataFromFile (user, "/home/np/projects/openni-nite/user-tracker/__build/UserCalibration.bin");
	skeleton_cap.StartTracking (user);
}

void
lost_user (xn::UserGenerator &generator, XnUserID user, void *pCookie)
{
	cout << "User #" << user << " lost" << endl;
}

void
depth_to_3d (XnPoint3D& pt, XnPoint3D& out)
{
	static const float view_angle = 2 * 3141.5927 / 360 * 63 / 640; // 1.82; /* milliradian per pix */
	
	out.X = sin((pt.X-320)*view_angle*1000) * pt.Z;
	out.Y = sin((pt.Y-240)*view_angle*1000) * pt.Z;
	out.Z = pt.Z;
}

void
gestures_process (XnPoint3D& lhand, XnPoint3D& rhand, XnPoint3D& head)
{
	XnPoint3D lhand3, rhand3, head3;
	depth_to_3d (lhand, lhand3);
	depth_to_3d (rhand, rhand3);
	depth_to_3d (head, head3);

	/*
	cout << "HD(" << (int)head.X << "," << (int)head.Y << "," << (int)head.Z << ") ";
	cout << "LH(" << (int)(lhand.X-head.X) << "," << (int)(lhand.Y-head.Y) << "," << (int)(lhand.Z-head.Z) << ") ";
	cout << "RH(" << (int)(rhand.X-head.X) << "," << (int)(rhand.Y-head.Y) << "," << (int)(rhand.Z-head.Z) << ") ";
	cout << endl;
	*/
}

int
main (int argc, char** argv)
{
	XnStatus err;
	xn::Context xn_context;

	/* Init context */
	EOE (err, xn_context.Init(), "Init context");

	if (argc > 1)
		EOE (err, xn_context.OpenFileRecording(argv[1]), "Open recording");
	
/*
	xn::EnumerationErrors errors;
	EOE (err, xn_context.InitFromXmlFile("/home/np/projects/openni-nite/user-tracker/__build/SamplesConfig.xml", &errors), "Init context");
*/

	/* Create DEPTH generator */
	xn::DepthGenerator depth_gen;
	EOE (err, depth_gen.Create (xn_context), "Create depth generator");

	/* Create IMAGE generator */
	xn::ImageGenerator image_gen;
	EOE (err, image_gen.Create (xn_context), "Create image generator");

	/* Create USER generator */
	xn::UserGenerator user_gen;
	XnCallbackHandle user_cb_hndl;
	EOE (err, user_gen.Create (xn_context), "Create user generator");
	EOE (err, user_gen.RegisterUserCallbacks (&detect_user, lost_user, &user_gen, user_cb_hndl), "Register user callbacks");

	xn::SkeletonCapability skeleton_cap = user_gen.GetSkeletonCap();
	skeleton_cap.SetSkeletonProfile (XN_SKEL_PROFILE_UPPER);


	xn::DepthMetaData depth_md;
	depth_gen.GetMetaData (depth_md);
	cout << "Depth width=" << depth_md.XRes() << ", height=" << depth_md.YRes() << endl;

	xn::ImageMetaData image_md;
	image_gen.GetMetaData (image_md);
	cout << "Image width=" << image_md.XRes() << ", height=" << image_md.YRes() << endl;


	/* Create output matrices */
	cv::Size depth_size (depth_md.XRes(), depth_md.YRes());
	cv::Size image_size (image_md.XRes(), image_md.YRes());
	cv::Mat depth_mat (depth_size, CV_16UC1);
	cv::Mat image_mat (image_size, CV_8UC3);
	cv::Mat depth_8u;
	

	/* Start generating */
	EOE (err, xn_context.StartGeneratingAll(), "Start generating");

	xn::SceneMetaData scene_md;
	cv::Mat user_mat (depth_size, CV_16UC1), user_8u (depth_size, CV_8UC1);

	XnPoint3DMovingAverage pointMA[6] = {
		XnPoint3DMovingAverage(3), XnPoint3DMovingAverage(3), XnPoint3DMovingAverage(3),
		XnPoint3DMovingAverage(3), XnPoint3DMovingAverage(3), XnPoint3DMovingAverage(3) };

	int runflag = 1;

	while (runflag) {
		EOE (err, xn_context.WaitOneUpdateAll(depth_gen), "Waiting");

		memcpy (depth_mat.data, depth_gen.GetDepthMap(), depth_size.width * depth_size.height * sizeof (XnDepthPixel));
		depth_mat.convertTo (depth_8u, CV_8UC1, 1./32.);

		memcpy (image_mat.data, image_gen.GetRGB24ImageMap(), image_size.width * image_size.height * sizeof (XnRGB24Pixel));
		cv::cvtColor (image_mat, image_mat, CV_RGB2BGR);

		XnUInt16 nusers = 10;
		XnUserID users[10];
		EOE (err, user_gen.GetUsers (users, nusers), "Get users");
		for (int iu = 0; iu < nusers; iu ++) {
			EOE (err, user_gen.GetUserPixels (users[iu], scene_md), "Get user pixels");
			memcpy (user_mat.data, scene_md.Data(), depth_size.width * depth_size.height * sizeof (XnLabel));
			user_mat.convertTo (user_8u, CV_8UC1, 64);

			depth_8u += user_8u;

			if (skeleton_cap.IsTracking (users[iu])) {
				XnSkeletonJointPosition joint;
				XnPoint3D point[6];

				skeleton_cap.GetSkeletonJointPosition (users[iu], XN_SKEL_LEFT_HAND, joint);
				point[0] = joint.position;

				skeleton_cap.GetSkeletonJointPosition (users[iu], XN_SKEL_RIGHT_HAND, joint);
				point[1] = joint.position;

				skeleton_cap.GetSkeletonJointPosition (users[iu], XN_SKEL_HEAD, joint);
				point[2] = joint.position;

				skeleton_cap.GetSkeletonJointPosition (users[iu], XN_SKEL_NECK, joint);
				point[3] = joint.position;

				skeleton_cap.GetSkeletonJointPosition (users[iu], XN_SKEL_TORSO, joint);
				point[4] = joint.position;

				skeleton_cap.GetSkeletonJointPosition (users[iu], XN_SKEL_WAIST, joint);
				point[5] = joint.position;

				depth_gen.ConvertRealWorldToProjective (6, point, point);

				for (int i = 0; i < 6; i ++)
					pointMA[i] << point[i];

				gestures_process (pointMA[0], pointMA[1], pointMA[2]);

				float zeroZ = (pointMA[2].point().Z + pointMA[3].point().Z + pointMA[4].point().Z + pointMA[5].point().Z) / 4.0;

				if ((zeroZ - pointMA[0].point().Z) > 300)
					cv::circle (depth_8u, cv::Point (10,10), 8, cv::Scalar (255), 2);
				if ((zeroZ - pointMA[1].point().Z) > 300)
					cv::circle (depth_8u, cv::Point (10,30), 8, cv::Scalar (255), 2);
				
/*
				if (isMoves (point[0], pointMA[0], false, false, true, 20.0))
					cv::circle (depth_8u, cv::Point (10,10), 8, cv::Scalar (255), 2);
				if (isMoves (point[1], pointMA[1], false, false, true, 20.0))
					cv::circle (depth_8u, cv::Point (10,30), 8, cv::Scalar (255), 2);
*/

				for (int i = 0; i < 6; i ++)
					point[i] = pointMA[i];

				cv::circle (depth_8u, cv::Point ((int)point[0].X, (int)point[0].Y), 5, cv::Scalar (255), 2);
				cv::circle (depth_8u, cv::Point ((int)point[1].X, (int)point[1].Y), 5, cv::Scalar (255), 2);
				cv::rectangle (depth_8u, cv::Rect ((int)point[2].X-5, (int)point[2].Y-5, 10, 10), cv::Scalar (255), 2);

				cv::circle (depth_8u, cv::Point ((int)point[3].X, (int)point[3].Y), 5, cv::Scalar (255), 2);
				cv::circle (depth_8u, cv::Point ((int)point[4].X, (int)point[4].Y), 5, cv::Scalar (255), 2);
				cv::circle (depth_8u, cv::Point ((int)point[5].X, (int)point[5].Y), 5, cv::Scalar (255), 2);
			} else
				cout << "No track skeleton" << endl;
		}


		cv::imshow ("depth", depth_8u);
		cv::imshow ("image", image_mat);
		cv::imshow ("user", user_8u);
		
		char c = (char)cv::waitKey (runflag == 1 ? 15 : 0);
		switch (c) {
			case ' ': runflag = runflag == 1 ? 2 : 1; break;
			case 'q': runflag = 0;
		}
	}

	return 0;
}
