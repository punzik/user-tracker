#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

class A {
protected:
	int n;
public:
	A (int n) : n (n) {}
	virtual void print() = 0;
};

class B : public A {
public:
	B (int n) : A (n) {}
	void print() {
		cout << "trulala " << n << endl;
	}
};

class C {
public:
	vector<A*> avec;

	void addA (A& a) {
		avec.push_back (&a);
	}
	
	void print() {
		for (vector<A*>::iterator it = avec.begin(); it != avec.end(); it ++)
			(*it)->print();
	}
};

int main(int argc, char** argv) {
	C c;
	
	B b1(1);
	B b2(2);
	
	c.addA (b1);
	c.addA (b2);
	c.print();
	
	return 0;
}

