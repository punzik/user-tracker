#include <vector>
#include <opencv2/opencv.hpp>

using namespace std;

void fsWritePoint3fVector (cv::FileStorage& fs, const vector<cv::Point3f>& vec)
{
	fs << "[";
	for (vector<cv::Point3f>::const_iterator it = vec.begin(); it != vec.end(); it ++) {
		fs << "{:";
		fs << "x" << it->x;
		fs << "y" << it->y;
		fs << "z" << it->z;
		fs << "}";
	}
	fs << "]";
}

#define WINSIZE		300
#define MAXC		1000

int
main (int argc, char** argv)
{
	if (argc < 3) {
		cout << "train-average <input.yaml> <output.yaml>" << endl << endl;
		return -1;
	}

	cv::FileStorage ifs (argv[1], cv::FileStorage::READ);
	if (!ifs.isOpened()) {
		cout << "Cannot open file " << argv[1] << endl;
		return -1;
	}

	cv::FileStorage ofs (argv[2], cv::FileStorage::WRITE);
	if (!ofs.isOpened()) {
		cout << "Cannot open file " << argv[1] << endl;
		return -1;
	}

	cv::Mat xy (cv::Size (WINSIZE,WINSIZE), CV_8UC3);	// front view
	cv::Mat zy (cv::Size (WINSIZE,WINSIZE), CV_8UC3);	// side view
	cv::Mat xz (cv::Size (WINSIZE,WINSIZE), CV_8UC3);	// top view

	cv::FileNode root = ifs.getFirstTopLevelNode();

	vector<cv::Point3f> average;

	int gestCount = 0;
	int points = 0;

	for (cv::FileNodeIterator i_ifs = root.begin(); i_ifs != root.end(); i_ifs ++) {
		/* READ GESTURE VECTOR */
		vector<cv::Point3f> gesture;

		for (cv::FileNodeIterator i_gest = (*i_ifs).begin(); i_gest != (*i_ifs).end(); i_gest ++) {
			cv::Point3f p;
			(*i_gest)["x"] >> p.x;
			(*i_gest)["y"] >> p.y;
			(*i_gest)["z"] >> p.z;

			gesture.push_back (p);
		}

		if (points == 0) {
			points = gesture.size();
			average.assign (points, cv::Point3f (0,0,0));
		}

		if (points != gesture.size()) {
			cout << "Gestures size not constant!" << endl;
			return -1;
		}

		/* Add interpolated gesture */
		for (int i = 0; i < points; i ++)
			average[i] += gesture[i];

		gestCount ++;
	}

	if (gestCount > 0) {
		for (int i = 0; i < points; i ++) {
			cv::Point3f p = average[i];
			p.x /= gestCount;
			p.y /= gestCount;
			p.z /= gestCount;
			average[i] = p;
		}

		ofs << root.name() << "[";
		fsWritePoint3fVector (ofs, average);
		ofs << "]";

		/* Show head point */
		cv::circle (xy, cv::Point (WINSIZE/2,WINSIZE/2), 6, cv::Scalar (255,255,255), 2);
		cv::circle (zy, cv::Point (WINSIZE/2,WINSIZE/2), 6, cv::Scalar (255,255,255), 2);
		cv::circle (xz, cv::Point (WINSIZE/2,WINSIZE/2), 6, cv::Scalar (255,255,255), 2);

		/* Show average gesture */
		for (int i = 0; i < points; i ++) {
			cv::Point3f ip = average.at (i);
			int x = (ip.x + MAXC) * WINSIZE / (MAXC*2);
			int y = (ip.y + MAXC) * WINSIZE / (MAXC*2);
			int z = (ip.z + MAXC) * WINSIZE / (MAXC*2);

			if (x < 0) x = 0;
			if (x >= WINSIZE) x = WINSIZE - 1;
			if (y < 0) y = 0;
			if (y >= WINSIZE) y = WINSIZE - 1;
			if (z < 0) z = 0;
			if (z >= WINSIZE) z = WINSIZE - 1;

			cv::circle (xy, cv::Point (x,y), 4, cv::Scalar (255,0,0), 2);
			cv::circle (zy, cv::Point (z,y), 4, cv::Scalar (0,255,0), 2);
			cv::circle (xz, cv::Point (x,z), 4, cv::Scalar (0,0,255), 2);
		}

		cv::imshow ("front", xy);
		cv::imshow ("side", zy);
		cv::imshow ("top", xz);

		cv::waitKey ();
	} else
		cout << "Gestures not found!" << endl;

	ofs.release();
	ifs.release();

	return 0;
}
