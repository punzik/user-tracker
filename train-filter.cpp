#include <vector>
#include <opencv2/opencv.hpp>

using namespace std;

void fsWritePoint3fVector (cv::FileStorage& fs, const vector<cv::Point3f>& vec)
{
	fs << "[";
	for (vector<cv::Point3f>::const_iterator it = vec.begin(); it != vec.end(); it ++) {
		fs << "{:";
		fs << "x" << it->x;
		fs << "y" << it->y;
		fs << "z" << it->z;
		fs << "}";
	}
	fs << "]";
}

static cv::Point3f interpolate (const vector<cv::Point3f>& gesture, int ilen, int in)
{
	int glen = gesture.size() - 1;
	
	float gin = (float)glen / (float)ilen * (float)in;
	int gin_a = floorf (gin);
	int gin_b = ceilf (gin);
	float di = gin - gin_a;

	cv::Point3f pa = gesture.at (gin_a);
	cv::Point3f pb = gesture.at (gin_b);

	cv::Point3f p;

	p.x = pa.x + (pb.x - pa.x) * di;
	p.y = pa.y + (pb.y - pa.y) * di;
	p.z = pa.z + (pb.z - pa.z) * di;

	return p;
}

#define WINSIZE		300
#define MAXC		1000

int
main (int argc, char** argv)
{
	if (argc < 3) {
		cout << "train-filter <input.yaml> <output.yaml> [interpolate-points]" << endl << endl;
		return -1;
	}

	cv::FileStorage ifs (argv[1], cv::FileStorage::READ);
	if (!ifs.isOpened()) {
		cout << "Cannot open file " << argv[1] << endl;
		return -1;
	}

	cv::FileStorage ofs (argv[2], cv::FileStorage::WRITE);
	if (!ofs.isOpened()) {
		cout << "Cannot open file " << argv[2] << endl;
		return -1;
	}

	cout << "Hotkeys:" << endl;
	cout << "a - add gesture to output set;" << endl;
	cout << "o - add all gestures;" << endl;
	cout << "q - quit;" << endl;
	cout << "any other key - skip gesture." << endl << endl;

	int interPoints;
	if (argc == 4)
		interPoints = atoi (argv[3]);
	else
		interPoints = 100;
	if (interPoints < 20) {
		cout << "Set interpolation points to 20" << endl;
		interPoints = 20;
	}

	cv::Mat xy (cv::Size (WINSIZE,WINSIZE), CV_8UC3);	// front view
	cv::Mat zy (cv::Size (WINSIZE,WINSIZE), CV_8UC3);	// side view
	cv::Mat xz (cv::Size (WINSIZE,WINSIZE), CV_8UC3);	// top view

	cv::FileNode root = ifs.getFirstTopLevelNode();

	ofs << root.name() << "[";

	bool doit = true;
	bool addAll = false;
	int addedGestures = 0;

	for (cv::FileNodeIterator i_ifs = root.begin(); i_ifs != root.end() && doit; i_ifs ++) {
		/* READ GESTURE VECTOR */
		vector<cv::Point3f> gesture;

		for (cv::FileNodeIterator i_gest = (*i_ifs).begin(); i_gest != (*i_ifs).end(); i_gest ++) {
			cv::Point3f p;
			(*i_gest)["x"] >> p.x;
			(*i_gest)["y"] >> p.y;
			(*i_gest)["z"] >> p.z;

			gesture.push_back (p);
		}

		/* Show head point */
		cv::circle (xy, cv::Point (WINSIZE/2,WINSIZE/2), 6, cv::Scalar (255,255,255), 2);
		cv::circle (zy, cv::Point (WINSIZE/2,WINSIZE/2), 6, cv::Scalar (255,255,255), 2);
		cv::circle (xz, cv::Point (WINSIZE/2,WINSIZE/2), 6, cv::Scalar (255,255,255), 2);

		vector<cv::Point3f> igesture;

		/* Interpolate gesture and show its */
		for (int i = 0; i < interPoints; i ++) {
			cv::Point3f ip = interpolate (gesture, interPoints, i);
			igesture.push_back (ip);

			int x = (ip.x + MAXC) * WINSIZE / (MAXC*2);
			int y = (ip.y + MAXC) * WINSIZE / (MAXC*2);
			int z = (ip.z + MAXC) * WINSIZE / (MAXC*2);

			if (x < 0) x = 0;
			if (x >= WINSIZE) x = WINSIZE - 1;
			if (y < 0) y = 0;
			if (y >= WINSIZE) y = WINSIZE - 1;
			if (z < 0) z = 0;
			if (z >= WINSIZE) z = WINSIZE - 1;

			cv::circle (xy, cv::Point (x,y), 4, cv::Scalar (255,0,0), 2);
			cv::circle (zy, cv::Point (z,y), 4, cv::Scalar (0,255,0), 2);
			cv::circle (xz, cv::Point (x,z), 4, cv::Scalar (0,0,255), 2);
		}

		/* Show noninterpolates gesture */
		for (vector<cv::Point3f>::iterator it = gesture.begin(); it != gesture.end(); it ++) {
			int x = (it->x + MAXC) * WINSIZE / (MAXC*2);
			int y = (it->y + MAXC) * WINSIZE / (MAXC*2);
			int z = (it->z + MAXC) * WINSIZE / (MAXC*2);

			if (x < 0) x = 0;
			if (x >= WINSIZE) x = WINSIZE - 1;
			if (y < 0) y = 0;
			if (y >= WINSIZE) y = WINSIZE - 1;
			if (z < 0) z = 0;
			if (z >= WINSIZE) z = WINSIZE - 1;

			cv::circle (xy, cv::Point (x,y), 4, cv::Scalar (100,100,100), 2);
			cv::circle (zy, cv::Point (z,y), 4, cv::Scalar (100,100,100), 2);
			cv::circle (xz, cv::Point (x,z), 4, cv::Scalar (100,100,100), 2);
		}


		cv::imshow ("front", xy);
		cv::imshow ("side", zy);
		cv::imshow ("top", xz);

		if (addAll) {
			fsWritePoint3fVector (ofs, igesture);
			addedGestures ++;
			cv::waitKey (15);
		} else {
			switch ((char)cv::waitKey (0)) {
			case 'o':
				addAll = true;
			case 'a':
				fsWritePoint3fVector (ofs, igesture);
				addedGestures ++;
				break;
			case 'q':
				doit = false;
				break;
			}
		}

		xy.setTo (cv::Scalar (0,0,0));
		zy.setTo (cv::Scalar (0,0,0));
		xz.setTo (cv::Scalar (0,0,0));
	}

	ofs.release();
	ifs.release();

	cout << "Added " << addedGestures << " gestures." << endl << endl;

	return 0;
}
