#include <vector>
#include <opencv2/opencv.hpp>

using namespace std;

int
main (int argc, char** argv)
{
	if (argc < 4) {
		cout << "train-filter <output.yaml> <input1.yaml> <input2.yaml> [input3.yaml] ..." << endl << endl;
		return -1;
	}

	cv::FileStorage ofs (argv[1], cv::FileStorage::WRITE);
	if (!ofs.isOpened()) {
		cout << "Cannot open file " << argv[2] << endl;
		return -1;
	}

	for (int i = 2; i < argc; i ++) {

		cv::FileStorage ifs (argv[i], cv::FileStorage::READ);
		if (!ifs.isOpened()) {
			cout << "Cannot open file " << argv[1] << endl;
			continue;
		}

		cv::FileNode root = ifs.getFirstTopLevelNode();

		ofs << root.name() << "[";

		for (cv::FileNodeIterator i_gs = root.begin(); i_gs != root.end(); i_gs ++) {
			ofs << "[";
			for (cv::FileNodeIterator i_vs = (*i_gs).begin(); i_vs != (*i_gs).end(); i_vs ++) {
				ofs << "{:";
				ofs << "x" << (float)(*i_vs)["x"];
				ofs << "y" << (float)(*i_vs)["y"];
				ofs << "z" << (float)(*i_vs)["z"];
				ofs << "}";
			}
			ofs << "]";
		}

		ofs << "]";

		ifs.release();
	}

	ofs.release();

	return 0;
}
