#include <vector>
#include <opencv2/opencv.hpp>

#include "skeleton.hpp"

using namespace std;

void fsWritePoint3fVector (cv::FileStorage& fs, const vector<cv::Point3f>& vec)
{
	fs << "[";
	for (vector<cv::Point3f>::const_iterator it = vec.begin(); it != vec.end(); it ++) {
		fs << "{:";
		fs << "x" << it->x;
		fs << "y" << it->y;
		fs << "z" << it->z;
		fs << "}";
	}
	fs << "]";
}

void gestureReceived (const vector<cv::Point3f>& gesture, void* priv)
{
	static int gn = 0;

	cv::FileStorage* fs = (cv::FileStorage*) priv;

	fsWritePoint3fVector (*fs, gesture);
	cout << "Received gesture " << gn++ << ", " << gesture.size() << " points" << endl;
}

int
main (int argc, char** argv)
{
	if (argc < 3) {
		cout << "train-record <filename.yaml> <gesture-name>" << endl << endl;
		return -1;
	}

	cv::FileStorage fs (argv[1], cv::FileStorage::WRITE);
	if (!fs.isOpened()) {
		cout << "Cannot open file " << argv[1] << endl;
		return -1;
	}

	cout << "Hotkeys:" << endl;
	cout << "q - quit;" << endl;

	fs << string (argv[2]) << "[";

	em::KinectController ctrl (4, "/home/np/projects/user-tracker/data/user-calibration.dat");

	ctrl.startControl();

	cv::Mat depth;
	//cv::Mat depth_8u;
	cv::Mat image;
	cv::Mat umask, umask_bgr;
	cv::Mat ltrack;
	cv::Mat rtrack;

	ltrack.setTo (cv::Scalar (0,0,0));
	rtrack.setTo (cv::Scalar (0,0,0));


	em::TrainableZGestureRecognizer gestLRecog;
	//em::TrainableGestureRecognizer gestRRecog;
	gestLRecog.setGestureReceivedCB (gestureReceived, &fs);
	//gestRRecog.setGestureReceivedCB (gestureReceived, (void*) "right hand");

	while (ctrl.processFrame()) {

		//ctrl.getImage (image);
		ctrl.getDepth (depth);
		
		ltrack.create (depth.rows, depth.cols, CV_8UC3);
		rtrack.create (depth.rows, depth.cols, CV_8UC3);
		umask.create (depth.rows, depth.cols, CV_8UC1);
		image.create (depth.rows, depth.cols, CV_8UC3);

		//depth.convertTo (depth_8u, CV_8UC1, 1./32.);

		umask.setTo (cv::Scalar (0));
		image.setTo (cv::Scalar (0,0,0));
		
		vector<em::User*> users = ctrl.getUsers ();
		for (vector<em::User*>::iterator it = users.begin(); it != users.end(); it ++) {
			em::User* user = *it;
			if (user->isTracking()) {
				user->getMask (umask);
				umask = umask * 128;
				cvtColor (umask, umask_bgr, CV_GRAY2BGR);
				image += umask_bgr;

				cv::Point lhp = user->getPointProj (em::SP_L_HAND);
				cv::Point rhp = user->getPointProj (em::SP_R_HAND);
				cv::Point cmp = user->getPointProj (em::SP_CHEST);

				cv::Point3f lh = user->getPointRel (em::SP_L_HAND);
				cv::Point3f rh = user->getPointRel (em::SP_R_HAND);

				if (gestLRecog.pushPoint (lh))
					cv::circle (ltrack, lhp, 5, cv::Scalar (255,0,0), 2);
				else
					ltrack.setTo (cv::Scalar (0,0,0));

				/*
				if (gestRRecog.pushPoint (rh))
					cv::circle (rtrack, rhp, 5, cv::Scalar (0,255,0), 2);
				else
					rtrack.setTo (cv::Scalar (0,0,0));
				*/

				cv::circle (image, lhp, 5, cv::Scalar (255,0,0), 2);
				cv::circle (image, rhp, 5, cv::Scalar (0,255,0), 2);
				cv::circle (image, cmp, 5, cv::Scalar (0,0,255), 2);

				image = image + ltrack + rtrack;
			}
		}

		//cv::imshow ("depth", depth_8u);
		cv::imshow ("image", image);
		//cv::imshow ("mask", umask);

		if ((char)cv::waitKey(5) == 'q')
			break;
	}

	fs << "]";
	fs.release ();

	return 0;
}
