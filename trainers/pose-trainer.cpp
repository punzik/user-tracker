#include <vector>
#include <opencv2/opencv.hpp>

#include "../skeleton.hpp"

using namespace std;

const int angleCount = 8;
const string angleNames[angleCount] = {
	"L_FOREARM",
	"R_FOREARM",
	"L_SHOULDER",
	"R_SHOULDER",
	"L_SHOULDER_TORSO",
	"R_SHOULDER_TORSO",
	"HEAD_TORSO",
	"TORSO_ABS"
};
bool angles[angleCount];

const int poseTimeout = 20;
const float angleThreshold = 0.1;

void poseDetect (em::User* user, void* priv)
{
	static int poseCounter = 0;

	cv::FileStorage &fs = *((cv::FileStorage*)priv);
	
	stringstream sb;
	sb << "p" << poseCounter ++;

	cout << sb.str() << endl;

	fs << sb.str() << "{:";
	for (int i = 0; i < angleCount; i ++)
		if (angles[i])
			fs << angleNames[i] << user->getAngle ((em::SkelAngle)i);
	fs << "}";
}

void printUsage ()
{
	cout << "pose-trainer <output.yaml> <pose-name> [list of angles]" << endl << endl;
	cout << "Posible angles:" << endl;
	for (int i = 0; i < angleCount; i ++)
		cout << angleNames[i] << endl;
	cout << endl;
}

int angleNumber (string name)
{
	int n = -1;
	for (int i = 0; i < angleCount; i ++)
		if (angleNames[i] == name) {
			n = i;
			break;
		}
	
	return n;
}

int main (int argc, char** argv)
{
	if (argc < 3) {
		printUsage ();
		return -1;
	}

	cv::FileStorage fs (argv[1], cv::FileStorage::WRITE);
	if (!fs.isOpened()) {
		cout << "Cannot open file " << argv[1] << endl;
		return -1;
	}

	string poseName = argv[2];
	
	if (argc == 3)
		for (int i = 0; i < angleCount; i ++)
			angles[i] = true;
	else
		for (int i = 3; i < argc; i ++) {
			int n = angleNumber (argv[i]);
			if (n >= 0)
				angles[n] = true;
		}
	
	em::KinectController ctrl (4, "/home/np/projects/user-tracker/data/user-calibration.bin");
	ctrl.setMirror (true);
	em::TrainablePoseRecognizer* poseRecog = new em::TrainablePoseRecognizer();
	poseRecog->setPoseDetectCB (poseDetect, 30, 0.1, &fs);
	poseRecog->addFakeTrainingSet ();
	ctrl.addRecognizer (poseRecog);
		
	ctrl.startControl();

	cv::Mat depth;
	cv::Mat image;

	fs << string (poseName) << "[";
	
	fs << "angles" << "[";
	for (int i = 0; i < angleCount; i ++)
		if (angles[i])
			fs << angleNames[i];
	fs << "]";
	
	while (ctrl.processFrame()) {

		ctrl.getImage (image);
		ctrl.getDepth (depth);
		
		cv::imshow ("depth", depth);
		cv::imshow ("image", image);

		if ((char)cv::waitKey(5) == 'q')
			break;
	}

	fs << "]";
	fs.release();

	return 0;
}
